----------------------------------------------------------------------
--INITIALISATION DES CLIENTS ... PLEASE CHILL AND WAIT FEW SECONDS....
----------------------------------------------------------------------

insert into client 
values 
(nextval('client_seq'), 'bark@mail.fr','Mike', 'SAO','0760069012'),
(nextval('client_seq'), 'sim@mail.fr','Dany', 'SER','0662269015'),
(nextval('client_seq'), 'test@mail.fr','Dams', 'BOU','0798579073'),
(nextval('client_seq'), 'smile@mail.fr','Mams', 'SER','0798579073'),
(nextval('client_seq'), 'tron@mail.fr','Dads','BOU','0645349012');

------------------------------------------------
--INSERTION DE SEQUENCECOLIS
------------------------------------------------
insert into sequencecolis
values
(nextval('code_seq')),
(nextval('code_seq')),
(nextval('code_seq')),
(nextval('code_seq')),
(nextval('code_seq'));

------------------------------------------------
--INSERTION DE COLIS
------------------------------------------------
insert into colis
values
(nextval('colis_seq'), '1.0', 1001,1,5),
(nextval('colis_seq'),'2.3',1002,1,5),
(nextval('colis_seq'), '1.8',1003,1,5),
(nextval('colis_seq'),'1.6',1004,2,3),
(nextval('colis_seq'),'2.1',1005,3,1);

------------------------------------------------
--INSERTION DE COMPTE
------------------------------------------------
insert into compte
values
(nextval('compte_seq'), 't1','123'),
(nextval('compte_seq'), 't2','123'),
(nextval('compte_seq'), 't3','123'),
(nextval('compte_seq'), 't4','123'),
(nextval('compte_seq'), 't5','123');



------------------------------------------------
--INSERTION DE TRANSPORTEUR
------------------------------------------------
insert into transporteur
values 
(nextval('transport_seq'),'poste@mail.fr','La Poste', 'SA',  '0760069012',1 ),
(nextval('transport_seq'), 'demeco@mail.fr','DEMECO', 'SA','0662269015',2 ),
(nextval('transport_seq'), 'logilow@mail.fr','LogiLow','SA','0798579073',3),
(nextval('transport_seq'), 'prevote@mail.fr','Prévoté','SCI','0798579073',4),
(nextval('transport_seq'), 'ups@mail.fr','UPS', 'SA','0645349012',5);


------------------------------------------------
--INSERTION DE RELAIS
------------------------------------------------
insert into relais
values 
(nextval('relais_seq'),'10 rue Aimee', '18600','10:00:00','19:00:00','FRANCE', '0760069012','PARIS'),
(nextval('relais_seq'), '1 rue Liberte', '80000','10:00:00','18:00:00','FRANCE','0662269015','AMIENS'),
(nextval('relais_seq'), '9 rue Paul Vaillant Couturier','60110','10:00:00','18:00:00','FRANCE','0798579073','AMIENS'),
(nextval('relais_seq'), '9 rue Paul Vaillant Couturier','60110','10:00:00','18:30:00','FRANCE','0798579073','AMIENS'),
(nextval('relais_seq'), '7 rue des acacias','60000','10:00:00','19:00:00','BEAUVAIS','FRANCE','BEAUVAIS');

----------------------------------------------------
--INSERTION DE LIVRAISONS (id colis, id transporteur
----------------------------------------------------

insert into livraison
values 
(nextval('livraison_seq'),'code',1,3),
(nextval('livraison_seq'),'code',2,3),
(nextval('livraison_seq'),'code',3,3),
(nextval('livraison_seq'),'code',4,4),
(nextval('livraison_seq'),'code',5,5);

-------------------------------------------------------------
--INSERTION DE LISTERELAISPARCOURUS (id livraison, id relais)
-------------------------------------------------------------

insert into listerelaisparcourus
values 
(1,1),
(1,2),
(1,3),
(2,4),
(2,5),
(3,1),
(3,5),
(4,1),
(4,2),
(4,3),
(4,4),
(5,3),
(5,4),
(5,5);


-------------------------------------------------------------
--INSERTION DE HEURELIVRAISON (id livraison, id relais)
-------------------------------------------------------------

insert into heurelivraison
values 
(nextval('heure_seq'),'25-11-2020 12:43:39','statut 1',1),
(nextval('heure_seq'),'26-11-2020 15:30:39','statut 2',1),
(nextval('heure_seq'),'28-11-2020 17:43:39','statut 3',1),
(nextval('heure_seq'),'25-11-2020 12:43:39','statut 1',2),
(nextval('heure_seq'),'26-11-2020 15:30:39','statut 2',2),
(nextval('heure_seq'),'28-11-2020 17:43:39','statut 3',2),
(nextval('heure_seq'),'25-11-2020 12:43:39','statut 1',3),
(nextval('heure_seq'),'26-11-2020 15:30:39','statut 2',3),
(nextval('heure_seq'),'28-11-2020 17:43:39','statut 3',3),
(nextval('heure_seq'),'25-11-2020 12:43:39','statut 1',4),
(nextval('heure_seq'),'26-11-2020 15:30:39','statut 2',4),
(nextval('heure_seq'),'28-11-2020 17:43:39','statut 3',4),
(nextval('heure_seq'),'25-11-2020 12:43:39','statut 4',4),
(nextval('heure_seq'),'26-11-2020 15:30:39','statut 1',5),
(nextval('heure_seq'),'28-11-2020 17:43:39','statut 2',5),
(nextval('heure_seq'),'30-11-2020 12:43:39','statut 3',5);

