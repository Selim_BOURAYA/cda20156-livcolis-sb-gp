
############                    LivColis                    ##############

	v.1
	07 décembre 2020
	par Selim et Gregory


Lien GitHuB du projet :

https://Selim_BOURAYA@bitbucket.org/Selim_BOURAYA/cda20156-livcolis-sb-gp.git


Bienvenue sur le site LivColis !

Celle-ci va vous permettre de gérer vos livraisons et de mettre en relation vos clients, mais aussi d'administer votre réseau de relais.
En vous connectant, grâce à vos identifiants vous pourrez :

	- Ajouter et gérer les utilisateurs :  affichage des client via une liste. Possibilité de modification des informations et de suppression du client via les boutons correspondant
		
						un lien permet en bas de page permet d'ajouter des utilisateurs via un formulaire
						Un bug empêche la suppression du client depuis la liste
	
	- Envoyer et suivre les colis : affichage des livraisons en cours avec la possibilité de supprimer la livraison, de suivre les étapes de la livraison et d'actualiser la position de la livraison via une liste
					Pour faire évoluer le statut de livraison, il est nécessaire de cliquer sur "suivre", puis sur la page suivante, sur "Réceptionner le colis"
					Le statut sera alors actualisé

	- Gérer les points relais : affichage des relais enregistrés et de leurs différentes informations. Il est possible de modifier les informations du relai sélectionner via le bouton "Modifier"
				    et d'ajouter un relais via le bouton "ajouter relais"
				    Un bug empêche la suppression du relais depuis la liste
	
	- Se déconnecter

Vos clients pourront également, via la page d'accueil, consulter le statut de leur livraison en renseignant le code de livraison reçu par mail dans le formulaire correspondant



Prérequis pour executer l'application : 

Avant de lancer l'application, assurez vous de placer le fichier .war à la racine du dossier webapps de votre serveur TomCat

Il sera de plus nécessaire, pour générér la base de données postgreSQl.
Pour cela, il est necessaire de créer, via pgAdmin, une base de donnée  ;
		
	nom de la base de donnée : livcolis
	port local : 5432
	
et un utilisateur ;
	
	username : selimGreg
	password : admin

	
Le script "Script-livColis.sql", présent dans le dossier Script SQL contenu dans le dossier livrables, peut être executé 
depuis DBeaver une fois la base générée lors du premier lancement.


Authentification:

Le transporteur doit entrer son login et son mot de passe.
S'il se connecte pour la première fois, il doit créer un compte en cliquant sur le bouton nouvel utilisateur et renseigner l'ensemble des champs.
Si le mail et le login sont déja présent dans la base de données, celle-ci renverra une erreur (ajout d'un contrôle nécessaire)

Une fois le nouvel utilisateur créé, un retour à l'écran d'authentification est effectué, et l'utilisateur doit rentrer son login et son mot de passe

Liste des login et mot de passe Transporteur (pour authentification sur base de données test)

(login - mot de passe)

poste  123
demeco  123
logic  123
prevote  123
ups  123



