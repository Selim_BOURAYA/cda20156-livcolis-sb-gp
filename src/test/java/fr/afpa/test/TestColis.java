package fr.afpa.test;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.mmnaseri.utils.spring.data.dsl.factory.RepositoryFactoryBuilder;

import fr.afpa.beans.Client;
import fr.afpa.beans.Colis;
import fr.afpa.model.GestionClient;
import fr.afpa.model.GestionColis;
import fr.afpa.repository.dao.ClientRepository;
import fr.afpa.repository.dao.ColisRepository;
import junitx.framework.ListAssert;

@RunWith(MockitoJUnitRunner.class)
public class TestColis {

	@Mock
	private ColisRepository colisRepository;

	@InjectMocks
	private GestionColis gestionColis;

	private boolean verif;
	private Client emetteur;
	private Client destinataire;
	private Colis colis;
	private Colis colisModif;
	

	@Test
	public void testAddColis() {

		ClientRepository mockClientRepository = RepositoryFactoryBuilder.builder().mock(ClientRepository.class);
				
		Client clientTest1 = new Client(1,"test");		
		Client clientTest2 = new Client(2,"test2");
		
		mockClientRepository.save(clientTest1);
		mockClientRepository.save(clientTest2);
				
				
		Optional<Client> emetteurBDD = mockClientRepository.findById((clientTest1.getId()));
		Optional<Client> destinataireBDD = mockClientRepository.findById((clientTest2.getId()));
				
		if(emetteurBDD != null && destinataireBDD != null && emetteurBDD != destinataireBDD) {
			
			emetteur = emetteurBDD.get();
			destinataire = destinataireBDD.get();
			
			colis = new Colis(Float.parseFloat("2"));
			colis.setEmetteur(emetteur);
			colis.setDestinataire(destinataire);
			colisRepository.save(colis);
			
			verif = true;
		}
		
		assertEquals(true, verif);
		
	}

	@Test
	public void testListColis() {

		ColisRepository mockColisRepository = RepositoryFactoryBuilder.builder().mock(ColisRepository.class);

		colis = new Colis(2f);
		ArrayList<Colis> listColisTest = new ArrayList<Colis>();
		listColisTest.add(colis);
		
		mockColisRepository.save(colis);
		List<Colis> listeColisRepo = mockColisRepository.findAll();

		assertEquals(listColisTest.get(0), listeColisRepo.get(0));
	}

	@Test
	public void testModifColis() throws Exception {

		ColisRepository mockColisRepository = RepositoryFactoryBuilder.builder().mock(ColisRepository.class);

		colis = new Colis(1,2f);
		mockColisRepository.save(colis);

		Long idC = colis.getId();
		Optional<Colis> colisBdd = mockColisRepository.findById(idC);

		if (colisBdd != null) {
			
			colisBdd.get().setPoids(3f);
			mockColisRepository.saveAndFlush(colisBdd.get());
		}
		
		Optional<Colis> colisBddModif = mockColisRepository.findById(idC);
		
		if (colisBddModif != null) {
			
			colisModif = colisBddModif.get();
		}
			

		List<Colis> listeColis = new ArrayList<Colis>();
		
		colis.setPoids(3f);
		listeColis.add(colis);

		assertTrue(listeColis.get(0).getPoids() == colisModif.getPoids());
	}

	@Test
	public void testSuppColis() {

		ColisRepository mockColisRepository = RepositoryFactoryBuilder.builder().mock(ColisRepository.class);
		
		Colis saveColis2 = new Colis(1,2f);
		mockColisRepository.save(saveColis2);
		
		mockColisRepository.deleteById((long)1);
		mockColisRepository.flush();
				
		List<Colis> listeColis = new ArrayList<Colis>();
		assertEquals(listeColis, mockColisRepository.findAll());

	}

	@BeforeAll
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterAll
	public static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	public void setUp() throws Exception {

	}

	@AfterEach
	public void tearDown() throws Exception {

	}

}
