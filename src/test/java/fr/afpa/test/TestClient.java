package fr.afpa.test;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.mmnaseri.utils.spring.data.dsl.factory.RepositoryFactoryBuilder;

import fr.afpa.beans.Client;
import fr.afpa.model.GestionClient;
import fr.afpa.repository.dao.ClientRepository;
import junitx.framework.ListAssert;

@RunWith(MockitoJUnitRunner.class)
public class TestClient {

	@Mock
	private ClientRepository clientRepository;

	@InjectMocks
	private GestionClient gestionClient;

	private Client client;
	private Client clientModif;
	private String verif;
	@Test
	public void testAddClient() {

		verif = gestionClient.addClient("prenom", "nom", "mail", "telephone");
		assertEquals("listeClient", verif);

	}

	@Test
	public void testListClient() {

		ClientRepository mockClientRepository = RepositoryFactoryBuilder.builder().mock(ClientRepository.class);

		client = new Client(1,"test");
		ArrayList<Client> listClientTest = new ArrayList<Client>();
		listClientTest.add(client);

		
		mockClientRepository.save(client);
		List<Client> listeClientRepo = mockClientRepository.findAll();

		assertEquals(listClientTest.get(0), listeClientRepo.get(0));
	}

	@Test
	public void testModifClient() throws Exception {

		ClientRepository mockClientRepository = RepositoryFactoryBuilder.builder().mock(ClientRepository.class);

		client = new Client (1,"test");
		mockClientRepository.save(client);

		Long idC = client.getId();
		Optional<Client> clientBdd = mockClientRepository.findById(idC);

		if (clientBdd != null) {
			
			clientBdd.get().setNom("newNom");
			mockClientRepository.saveAndFlush(clientBdd.get());
		}
		
		Optional<Client> clientBddModif = mockClientRepository.findById(idC);
		
		if (clientBddModif != null) {
			
			clientModif = clientBddModif.get();
		}
			

		List<Client> listeClient = new ArrayList<Client>();
		client.setNom("newNom");
		listeClient.add(client);

		assertEquals(listeClient.get(0).getNom(), clientModif.getNom());
	}

	@Test
	public void testSuppClient() {

		ClientRepository mockClientRepository = RepositoryFactoryBuilder.builder().mock(ClientRepository.class);
		
		Client saveClient2 = new Client("prenom", "nom", "mail", "telephone");
		mockClientRepository.save(saveClient2);
		
		mockClientRepository.deleteById(saveClient2.getId());
		mockClientRepository.flush();
				
		List<Client> listeClient = new ArrayList<Client>();
		
		assertEquals(listeClient, mockClientRepository.findAll());

	}

	@BeforeAll
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterAll
	public static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	public void setUp() throws Exception {

	}

	@AfterEach
	public void tearDown() throws Exception {

	}

}
