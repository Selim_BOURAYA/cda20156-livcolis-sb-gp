package fr.afpa.test;

import static org.junit.Assert.assertEquals;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.mmnaseri.utils.spring.data.dsl.factory.RepositoryFactoryBuilder;

import fr.afpa.beans.Colis;
import fr.afpa.beans.Compte;
import fr.afpa.beans.HeureLivraison;
import fr.afpa.beans.Livraison;
import fr.afpa.beans.Relais;
import fr.afpa.beans.Transporteur;
import fr.afpa.model.GestionClient;
import fr.afpa.model.GestionTransporteur;
import fr.afpa.repository.dao.ClientRepository;
import fr.afpa.repository.dao.ColisRepository;
import fr.afpa.repository.dao.CompteRepository;
import fr.afpa.repository.dao.HeureLivRepository;
import fr.afpa.repository.dao.LivraisonRepository;
import fr.afpa.repository.dao.RelaisRepository;
import fr.afpa.repository.dao.TransporteurRepository;
import fr.afpa.utils.Utilitaires;



@RunWith(MockitoJUnitRunner.class)
public class TransportTest {	

	@Mock
	private TransporteurRepository transporteurRepository; 
	
	@Mock
	private CompteRepository compteRepository;
	
	@Mock
	private LivraisonRepository livraisonRepository;
	
	@Mock
	private ColisRepository colisRepository;
	
	
	@InjectMocks
	private GestionTransporteur gestionTransport;
	

	@Mock
	private ClientRepository clientRepository; 
	
	@InjectMocks
	private GestionClient gestionClient;
	
	private String verif;
	private Colis colis = new Colis();
	private Livraison liv = new Livraison();
	private Relais relais = new Relais();
	private HashSet<Relais> listeRelais = new HashSet <Relais> ();
	private HeureLivraison heureLivraison; 
	private Compte compte = new Compte("login", "password");
	private Transporteur transporteur = new Transporteur("prenom", "nom", "mail", "telephone", compte);
	private List <Livraison> listeLivraison  = new ArrayList<Livraison>();
	
	
	@Test
	public void testAddTransporteur() {
		
		assertEquals( "ajout réussi", gestionTransport.addTransporteur("testNom","testPrenom","testMail",
				"testTelephone", "testLogin", "testPassword"));
	
		
	}
	

/*	@Test
	public void testBuildLivraison() {
		
		verif = gestionTransport.buildLivraison(verif, colis, listeRelais, heureLivraison);
		assertEquals( "livraison ajouté", verif );
		
	}*/

	@Test
	public void testLivraisonList() {

		final TransporteurRepository transporteurRepo = RepositoryFactoryBuilder.builder().mock(TransporteurRepository.class);
		final LivraisonRepository livraisonRepository = RepositoryFactoryBuilder.builder().mock(LivraisonRepository.class);
		
		compte = new Compte("login","mdp"); 
		Transporteur transporteur1 = new Transporteur(1L,"testPrenom","testNom","testMail","testTelephone", compte);
		
		List<Livraison> listeLivraison = new ArrayList<Livraison>();
		Livraison livraison = new Livraison(transporteur1);
				
		livraisonRepository.save(livraison);
		
		transporteur1.setListeLivraison(listeLivraison);
		transporteurRepo.save(transporteur1);
		
		List<Livraison> listeLivraisonTransporteur = livraisonRepository.findByTransporteur(transporteur1);
		
		assertEquals(listeLivraison, listeLivraisonTransporteur);
				
	}
	
	
	@Test
	public void testAuth() {
	
		final CompteRepository compterepo = RepositoryFactoryBuilder.builder().mock(CompteRepository.class);
		
		compte = new Compte("login","mdp"); 
		Transporteur transporteur1 = new Transporteur(); 
		
		compterepo.save(compte);
		
		Compte compte1 = compterepo.findByLoginAndPassword("login", "mdp"); 
		transporteur1 = new Transporteur("testPrenom","testNom","testMail",
				"testTelephone", compte1);
		
		assertEquals( "mdp", transporteur1.getCompte().getPassword());
		
	}
	
	
	@Test
	public void testAddRelaisToLivrais() {
		LocalTime open = LocalTime.now();
		
		final RelaisRepository  relaisRepository = RepositoryFactoryBuilder.builder().mock(RelaisRepository.class);
		final ColisRepository colisRepository = RepositoryFactoryBuilder.builder().mock(ColisRepository.class);
		final LivraisonRepository livraisonRepository = RepositoryFactoryBuilder.builder().mock(LivraisonRepository.class);
		
		Livraison liv = new Livraison();
		Livraison liv1 = new Livraison();
		
		relais = new Relais("TestAddresse","testcode","ville","pays","telephone",open,open);
		relaisRepository.save(relais);
		
		colis = new Colis();
		colisRepository.save(colis);
		
		liv = new Livraison(colis);
		
		listeRelais.add(relais);
	
		liv.setListeRelais(listeRelais);
		
		livraisonRepository.save(liv);
		liv1 = livraisonRepository.findByColis(colis);
		
		
		
		
		
		//assertEquals("TestAddresse",liv1.getListeRelais().get(0).getAdresse());
		
	}
	
	
	@Test
	public void testModifStatutliv() {
		
		final ColisRepository colisRepository = RepositoryFactoryBuilder.builder().mock(ColisRepository.class);
		final LivraisonRepository livraisonRepository = RepositoryFactoryBuilder.builder().mock(LivraisonRepository.class);
		final HeureLivRepository heurelivraisonRepository = RepositoryFactoryBuilder.builder().mock(HeureLivRepository.class);
		
		heureLivraison = new HeureLivraison();
		Livraison liv1 = new Livraison();
		String heuredeliv = Utilitaires.addHeure();
		HashSet <HeureLivraison> listHeure = new HashSet<HeureLivraison>();
		
		heureLivraison.setDateLivraison(heuredeliv);
		heureLivraison.setStatut("testStatut");
		heurelivraisonRepository.save(heureLivraison);
		
		listHeure.add(heureLivraison);
		
		colis = new Colis();
		colisRepository.save(colis);
		Livraison liv = new Livraison(colis);
		
		liv.setListeHeureReception(listHeure);
		livraisonRepository.save(liv);
		liv1  = livraisonRepository.findByColis(colis);
		
		//assertEquals("testStatut",liv1.getListeHeureReception().get(0).getStatut());
		
	}
	
	@Test
	public void mailTest() {
		
		String mail1 = "gregorypounah@yahoo.fr";
		
		
		assertEquals("mail envoyé", gestionTransport.envoiMail(liv,relais,mail1,mail1));
	}

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		
		
		
	}
	

	@After
	public void tearDown() throws Exception {
	}

	

}
