package fr.afpa.test;

import static org.junit.Assert.assertEquals;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Optional;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.mmnaseri.utils.spring.data.dsl.factory.RepositoryFactoryBuilder;

import fr.afpa.beans.Relais;
import fr.afpa.model.GestionRelais;
import fr.afpa.repository.dao.RelaisRepository;




@RunWith(MockitoJUnitRunner.class)
public class TestRelais {
	
	@Mock
	private RelaisRepository relaisRepository;
	
	
	@InjectMocks
	private GestionRelais gestionRelais;
	
	private Relais relais;
	private Relais relais1;
	private Relais relais2;
	
	
	
	@Test
	public void testAddRelais() {
		LocalTime ouverture = LocalTime.now();
		LocalTime fermeture = LocalTime.now();
		
		assertEquals("constante relais ajouté",gestionRelais.addRelais("adresse", "code", "ville", "pays", "telephone", ouverture, fermeture));
	}
	
	@Test
	public void testListerRelais() {
		
		LocalTime ouverture = LocalTime.now();
		LocalTime fermeture = LocalTime.now();
		
		final RelaisRepository relaisrepo = RepositoryFactoryBuilder.builder().mock(RelaisRepository.class);
		
		relais = new Relais("adresse", "code", "ville", "pays", "telephone", ouverture, fermeture);
		relaisrepo.save(relais);
		
		java.util.List<Relais> listerelai1 =(ArrayList<Relais>) new ArrayList< Relais>();
		
		
		listerelai1 = relaisrepo.findAll();
		
		 String addresse = listerelai1.get(0).getAdresse();
		assertEquals("adresse",addresse);
		
	}
	
	@Test
	public void testModifRelais() {
		
		LocalTime ouverture = LocalTime.now();
		LocalTime fermeture = LocalTime.now();
		
		final RelaisRepository relaisrepo = RepositoryFactoryBuilder.builder().mock(RelaisRepository.class);
		relais = new Relais("adresse", "code", "ville", "pays", "telephone", ouverture, fermeture);
		relais1 = new Relais();
		
		relaisrepo.save(relais);
		Long idrelais = relais.getId();
		
		Optional <Relais> relaistest = relaisrepo.findById(idrelais);
		if (relaistest!=null) {
			
			relaistest.get().setAdresse("adresseTest");
			relaisrepo.saveAndFlush(relaistest.get());
			
		}
		Optional <Relais> relaistest1 = relaisrepo.findById(idrelais);
		if(relaistest1!=null) {
			relais1 = relaistest1.get();
		}
		
		assertEquals("adresseTest",relais1.getAdresse());
	}
	
	@Test
	public void suppRelais() {
		
		LocalTime ouverture = LocalTime.now();
		LocalTime fermeture = LocalTime.now();
		relais = new Relais("adresse", "code", "ville", "pays", "telephone", ouverture, fermeture);
		String test = "supp réussi";
		final RelaisRepository relaisrepo = RepositoryFactoryBuilder.builder().mock(RelaisRepository.class);
		
		relaisrepo.save(relais);
		
		Long idrelais = relais.getId();
		
		Optional <Relais> relaistest1 = relaisrepo.findById(idrelais);
		
		if(relaistest1!=null) {
			relais1 = relaistest1.get();
		}
		
		/*
		 * La méthode deleteById de JPA repository ne fonctionne pas en TEST avec le framework mock 
		 * obligé de faire les test avec delete pour les rendres fonctionels 
		 * méthode deleteById utiliser pour la GestionRelais
		 * */
		//relaisrepo.deleteById(idrelais);
		
		relaisrepo.delete(relais);
		
		Optional <Relais> relaistest = relaisrepo.findById(idrelais);
		
		if (relaistest!=null) {
			
			test = "supp réussi";
		}
		
		assertEquals("supp réussi",test);
		//assertEquals(relais1.getAdresse(),relais2.getAdresse());
	}

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	

}
