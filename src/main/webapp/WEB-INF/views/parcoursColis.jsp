<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
    <%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link href="${pageContext.request.contextPath}/resources/style/style.css" rel="stylesheet" type="text/css">
  <link href="${pageContext.request.contextPath}/resources/css/style.css" rel="stylesheet" type="text/css">
   <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

<title>Statut du colis</title>

</head>
<body>

	<div class="main_nav" center>
      <nav class="navbar fixed-top navbar-expand-lg navbar-light" style="background-color: #C0D1E8;" >
             <a class="navbar-brand" href="#">Liv colis</a>
         <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
           <span class="navbar-toggler-icon"></span>
         </button>

         <div class="collapse navbar-collapse" id="navbarSupportedContent" >
           <ul class="navbar-nav mr-auto">
             <li class="nav-item active">
               <a href="<c:out value ="${pageContext.request.contextPath}/Deconnexion"/>">Se déconnecter</a>
             </li>
          </ul>



         </div>
       </nav>

  </div>
	
	<div class="position-relative p-3 p-md-5 m-md-3 text-center">


	<h1>Bienvenue ${sessionScope.transporteur.nom}</h1>
	<br>

    <tr>
        <td><c:out value="${livraison.colis.id}" /> id colis</td>
    </tr>

	<div class = "tableaux">
    <TABLE class="minimalist">

        <TR>
            <TH><a>  date de reception </a></TH>
            <TH><a>  statut </a></TH>
       </TR>

            <c:forEach items="${listeStatut}" var="statut">
                    <tr>
                        <td><c:out value="${statut.dateLivraison}" /></td>
                        <td><c:out value="${statut.statut}" /></td>
                    </tr>
            </c:forEach>
     </TABLE>
     </div>

   	<div class = "tableaux">
    <TABLE class="minimalist">
        <TR>

            <TH><a>  localisation </a></TH>
            <TH><a>  Action </a></TH>
        </TR>

        <c:forEach items="${listeRelais}" var="relais">
                <tr>
                    <td><c:out value="${relais.ville}" /></td>
                    <td><a	href="${pageContext.request.contextPath}/reception/${livraison.id}">Receptionner le colis</button></a></td>
                </tr>
        </c:forEach>
     </TABLE>
     </div>
	<br>
	
<div class="position-relative p-3 p-md-5 m-md-3 text-center">
	
     <a	href="${pageContext.request.contextPath}/">
        <button type="button" name="retour" value="retour">Retour à la page d'accueil</button></a>
           <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>    
	</div>
</body>
</html>
