<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
      <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link href="${pageContext.request.contextPath}/resources/style/style.css" rel="stylesheet" type="text/css">
   <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<title>Livraison</title>
</head>
<body>
  <div class="main_nav" center>
      <nav class="navbar fixed-top navbar-expand-lg navbar-light"  style="background-color: #C0D1E8;">
             <a class="navbar-brand" href="#">Liv colis</a>
         <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
           <span class="navbar-toggler-icon"></span>
         </button>

         <div class="collapse navbar-collapse" id="navbarSupportedContent" >
           <ul class="navbar-nav mr-auto">
             <li class="nav-item active">
               <a href="<c:out value ="${pageContext.request.contextPath}/Deconnexion"/>">Se déconnecter</a>
             </li>
          </ul>



         </div>
       </nav>

       <br>
         <br>
  </div>

  <br><br>
<div class="position-relative p-3 p-md-5 m-md-3 text-center">

		<h1>Bienvenue ${sessionScope.transporteur.nom}</h1><br>


			<table class="minimalist">
			<CAPTION>Liste de colis en cours</CAPTION>

			<thead>
				<tr>
					<th>no_livraison</th>
					<th>code</th>
					<th>no_colis</th>
					<th>emetteur</th>
					<th>destinaire</th>
					<th>Supprimer</th>
					<th>Suivre</th>

				</tr>
			</thead>
			<tfoot>
				<tr>
					<td><br><br><a href="addcolis">Ajouter colis</a></td>
				</tr>
			</tfoot>
			<tbody>

			<c:set var="i" value="0" />
			<c:set var="produitASupprimer" />

					<c:forEach items="${listeLivraison}" var="livraison">
            <tr>

                  <td><c:out value="${livraison.id}" /></td>
                   <td><c:out value="${livraison.codeClient}" /></td>
                    <td><c:out value="${livraison.colis.id}" /></td>
                    <td><c:out value="${livraison.colis.emetteur.nom}" />  &nbsp;  <c:out value="${livraison.colis.emetteur.prenom}" /></td>
                    <td><c:out value="${livraison.colis.destinataire.nom}" /> &nbsp;  <c:out value="${livraison.colis.destinataire.prenom}" /></td>


                	<td><a href="${pageContext.request.contextPath}/deleteColisbyId/${livraison.id}">
						 <button type="button" name="supprimer" value="supprimer">Supprimer</button></a></td>

						 <td><a href="${pageContext.request.contextPath}/parcourscolis/${livraison.id}">
						 <button type="button" name="suivre" value="Suivre">Suivre</button></a></td>


            </tr>
    </c:forEach>
			</tbody>
		</table>


		<a href="${pageContext.request.contextPath}/espaceTranspo">
        <button type="button" name="retour accueil" value="retour accueil">Retour à l'accueil</button></a>
        
     <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>    
</body>
</html>
