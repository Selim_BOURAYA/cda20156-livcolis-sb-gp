<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
 
<link href="${pageContext.request.contextPath}/resources/style/style.css" rel="stylesheet" type="text/css">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">

<title>Ajout Utilisateur</title>
</head>
<body>
  <br>
  <br>
    <div class="main_nav" center>
        <nav class="navbar fixed-top navbar-expand-lg navbar-light" style="background-color: #C0D1E8;" >
               <a class="navbar-brand" href="#">Liv colis</a>
           <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
             <span class="navbar-toggler-icon"></span>
           </button>

           <div class="collapse navbar-collapse" id="navbarSupportedContent" >
             <ul class="navbar-nav mr-auto">
               <li class="nav-item active">
                 <a href="<c:out value ="${pageContext.request.contextPath}/Deconnexion"/>">Se déconnecter</a>
               </li>
            </ul>



           </div>
         </nav>

         <br>
           <br>
    </div>

    <br><br>
  <div class="position-relative p-3 p-md-5 m-md-3 text-center">

	    <h1>Ajout utilisateur</h1>
        <form action="addclient" method="post">
			<div class = "container">
				<label for="uname"><b>Entrez Nom : </b></label>
				<input type="text" placeholder="Entrez votre nom" name="nom" required><br>

				<label for="usurname"><b>Entrez Prénom : </b></label>
				<input type="text" placeholder="Entrez votre prenom" name="prenom"><br>


				<label for="uname"><b>Phone : </b></label>
				<input type="text" placeholder="Entrez votre numéro de tel" name="telephone" required >
				 <span class="erreur">${erreurs['noPhone']}</span>
                <br>


				<label for="usurname"><b>Mail : </b></label>
				<input type="text" placeholder="Entrez votre mail" name="mail">
				 <span class="erreur">${erreurs['mail']}</span>
                <br />
				 <p class="erreur">${resultat}</p>

				<button type="submit">Créer Utilisateur</button>

			</div>


		</form>


 <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>    
</body>
</html>
