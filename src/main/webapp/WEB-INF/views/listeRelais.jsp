<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
      <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link href="${pageContext.request.contextPath}/resources/style/style.css" rel="stylesheet" type="text/css">
   <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<title>Points Relais</title>
</head>
<body>

  <div class="main_nav" center>
      <nav class="navbar fixed-top navbar-expand-lg navbar-light " style="background-color: #C0D1E8;" >
             <a class="navbar-brand" href="#">Liv colis</a>
         <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
           <span class="navbar-toggler-icon"></span>
         </button>

         <div class="collapse navbar-collapse" id="navbarSupportedContent" >
           <ul class="navbar-nav mr-auto">
             <li class="nav-item active">
               <a href="<c:out value ="${pageContext.request.contextPath}/Deconnexion"/>">Se déconnecter</a>
             </li>
          </ul>



         </div>
       </nav>

       <br>
         <br>
  </div>

  <br><br>
<div class="position-relative p-3 p-md-5 m-md-3 text-center">
		<h1>Bonjour ${sessionScope.transporteur.nom}</h1><br>


			<table class="minimalist">
			<CAPTION>Liste de Relais</CAPTION>

			<thead>
				<tr>
					<th>no_Relais</th>
					<th>code</th>
					<th>adresse</th>
					<th>Ville</th>
					<th>Pays</th>
					<th>horraire ouvertures</th>
					<th>horraire fermetures</th>
					<th>Supprimer</th>
					<th>Modifier</th>


				</tr>
			</thead>
			<tfoot>
				<tr>
					<td><br><br><a href="addRelais">Ajouter relais</a></td>
				</tr>
			</tfoot>
			<tbody>

			<c:set var="i" value="0" />
			<c:set var="produitASupprimer" />

					<c:forEach items="${listeRelais}" var="relais">
            <tr>

                  <td><c:out value="${relais.id}" /></td>
                   <td><c:out value="${relais.code}" /></td>
                    <td><c:out value="${relais.adresse}" /></td>
                     <td><c:out value="${relais.ville}" /></td>
                      <td><c:out value="${relais.pays}" /></td>
                       <td><c:out value="${relais.ouverture}" /></td>
                        <td><c:out value="${relais.fermeture}" /></td>


                	<td><a href="${pageContext.request.contextPath}/deleteRelaisById/${relais.id}">
						 <button type="button" name="supprimer" value="supprimer">Supprimer</button></a></td>

						 	<td><a href="${pageContext.request.contextPath}/updateRelaisById/${relais.id}">
						 <button type="button" name="Modifier" value="Modifier">Modifier</button></a></td>



            </tr>
    </c:forEach>
			</tbody>
		</table>

		<a href="${pageContext.request.contextPath}/espaceTranspo">
        <button type="button" name="retour accueil" value="retour accueil">Retour à l'accueil</button></a>
    </div>
</body>

 <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>    
</html>
