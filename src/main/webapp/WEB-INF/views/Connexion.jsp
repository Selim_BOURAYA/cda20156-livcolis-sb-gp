<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link href="${pageContext.request.contextPath}/resources/style/style.css" rel="stylesheet" type="text/css">
<link href="${pageContext.request.contextPath}/resources/css/style.css" rel="stylesheet" type="text/css">
 <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<title>Connexion</title>
</head>
<body>
		<div class="position-relative p-3 p-md-5 m-md-3 text-center">
		    <h1>Connexion transporteur</h1>
        <form action="ConnexionTransporteur" method="post">
			<div class = "container">

				<label for="uname"><b>Login : </b></label>
				<input type="text" placeholder="Entrez votre login" name="login" required>


				<label for="usurname"><b>Mot de Passe : </b></label>
				<input type="password" placeholder="Entrez votre mot de passe" name="password" required >



				 <p class="erreur">${resultat}</p>

				<button type="submit">Connexion compte</button>

			</div>
		</div>

		</form>
</body>
</html>
