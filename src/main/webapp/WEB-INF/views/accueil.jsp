<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
    <!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
<link href="${pageContext.request.contextPath}/resources/style/style.css" rel="stylesheet" type="text/css">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">

<title>LivColis - Accueil</title>
</head>
<body >

  
 
	  <nav class="navbar fixed-top navbar-expand-lg navbar-light" style="background-color: #C0D1E8;" >
             <a class="navbar-brand" href="#">Liv colis</a>
         <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
           <span class="navbar-toggler-icon"></span>
         </button>

         <div class="collapse navbar-collapse" id="navbarSupportedContent" >
           <ul class="navbar-nav mr-auto">
           	 <li class="nav-item active">
               <a href="<c:out value ="${pageContext.request.contextPath}/Connexion"/>"> se connecter</a>
                 &nbsp;
             </li>
             <li class="nav-item active">
                <a href="<c:out value ="${pageContext.request.contextPath}/creationCompte"/>"> créer un compte</a>
             </li>
          </ul>
		</div>
		</nav>



 <br>		
 
 		<div class="position-relative p-3 p-md-5 m-md-3 ">
 			<h1>Suivis de colis</h1>
 		</div>
	<br>

   <div class="fixed-bottom">
   	<div class="position-relative p-3 p-md-5 m-md-3 text-center">

            <form action="recherche" method="post">
            <label for="uname"><b>Entrez votre code colis : </b></label>
            <input type="text" name="codecolis" required >
            <button type="submit">Rechercher colis</button>
            </form>
  
        </div>
       </div> 
  
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>    
</body>
</html>
