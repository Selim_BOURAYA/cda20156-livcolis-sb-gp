<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
      <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link href="${pageContext.request.contextPath}/resources/style/style.css" rel="stylesheet" type="text/css" style="background-color: #C0D1E8;">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<title>Livraison</title>
</head>
<body>
		<h1>Bonjour ${sessionScope.transporteur.nom}</h1><br>


			<table class="minimalist">
			<CAPTION>Liste de colis en cours</CAPTION>

			<thead>
				<tr>
					<th>no_livraison</th>
					<th>code</th>
					<th>no_colis</th>
					<th>emetteur</th>
					<th>destinaire</th>
					<th>Ajouter � cette livraison</th>

				</tr>
			</thead>
			<tfoot>
				<tr>
					<td><br><br><a href="addcolis">Ajouter colis</a></td>
					<td><br><br><a href="./">Actualiser</a></td>
				</tr>
			</tfoot>
			<tbody>

			<c:set var="i" value="0" />
			<c:set var="produitASupprimer" />

					<c:forEach items="${listeLivraison}" var="livraison">
            <tr>

                  <td><c:out value="${livraison.id}" /></td>
                   <td><c:out value="${livraison.codeClient}" /></td>
                    <td><c:out value="${livraison.colis.id}" /></td>
                    <td><c:out value="${livraison.colis.emetteur.nom}" />  &nbsp;  <c:out value="${livraison.colis.emetteur.prenom}" /></td>
                    <td><c:out value="${livraison.colis.destinataire.nom}" /> &nbsp;  <c:out value="${livraison.colis.destinataire.prenom}" /></td>


                	<td><a href="${pageContext.request.contextPath}/AddRelaisLivraisonById/${livraison.id}/${relais.id}">
						 <button type="button" name="supprimer" value="supprimer">Ajouter � cette livraison</button></a></td>




            </tr>
    </c:forEach>
			</tbody>
		</table>


		<a href="${pageContext.request.contextPath}/espaceTranspo">
        <button type="button" name="retour accueil" value="retour accueil">Retour � l'accueil</button></a>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>    
</body>
</html>
