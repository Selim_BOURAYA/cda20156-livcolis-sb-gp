<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link href="${pageContext.request.contextPath}/resources/style/style.css" rel="stylesheet" type="text/css">
<link href="${pageContext.request.contextPath}/resources/css/style.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<title>Modifier Relais</title>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/style.css" />
</head>
<body>

	<div class="position-relative p-3 p-md-5 m-md-3 text-center">

	
	
	<h1>Bienvenue ${sessionScope.transporteur.nom}</h1>
	<br>
	
			<h1>Modifier relais</h1>
	<div class="position-relative p-3 p-md-3 m-md-3 text-center">
		
        <form action="${pageContext.request.contextPath}/updateRelais" method="post">
			

				<label for="uname">id relais :</label>
				<input type="text" id="relais_id" name="relais_id" value="${relais.id}"><br>

				<label for="uname"><b>Entrez adresse : </b></label>
				<input type="text" name="adresse" required><br>

				<label for="usurname"><b>Entrez Ville: </b></label>
				<input type="text" name="ville"><br>


				<label for="uname"><b>Entrez Pays : </b></label>
				<input type="text" name="pays" required ><br>


				<label for="uname"><b>Entrez telephone : </b></label>
				<input type="text" name="telephone" required ><br>

				<label for="uname"><b>Entrez code : </b></label>
				<input type="text" name="code" required ><br>

				 <label for="uname"><b>Horraire Ouverture (format "hh:mm") </b></label>
				<input type="text" name="horraireouvert" required ><br>

				 <label for="uname"><b>Horraire Fermeture (format "hh:mm")</b></label>
				<input type="text" name="horraireferme" required ><br>
                <br>



				<button type="submit">Ajouter Relais</button>

			
			</form>
		</div>
	</div>
	
     <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>     
</body>
</html>
