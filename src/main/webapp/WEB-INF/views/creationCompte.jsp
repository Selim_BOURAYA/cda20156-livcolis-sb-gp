<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
   <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
 <meta name="viewport" content="width=device-width, initial-scale=1">
<link href="${pageContext.request.contextPath}/resources/style/style.css" rel="stylesheet" type="text/css">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">

<div class="position-relative p-3 p-md-5 m-md-3   text-center">
<title>Création de compte</title>
</head>
<body>
		    <h1>Création transporteur</h1><br><br>
        <form action="CreationTransporteur" method="post">
			<div class = "container">
				<label for="uname"><b>Nom : </b></label>
				<input type="text" placeholder="Entrez votre nom" name="nom" required><br>

				<label for="usurname"><b>Prénom : </b></label>
				<input type="text" placeholder="Entrez votre prenom" name="prenom"><br>

				<label for="uname"><b>Login : </b></label>
				<input type="text" placeholder="Entrez votre login" name="login" required>
				 <span class="erreur">${erreurs['login']}</span>
                <br>

				<label for="usurname"><b>Mot de Passe : </b></label>
				<input type="password" placeholder="Entrez votre mot de passe" name="password" required >
				  <span class="erreur">${erreurs['password']}</span>
				<br>

				<label for="usurname"><b>Mot de Passe : </b></label>
				<input type="password" placeholder="Confirmer votre mot de passe" name="confirmpassword" required >
				  <span class="erreur">${erreurs['confirmpassword']}</span>
                <br />

				<label for="uname"><b>Phone : </b></label>
				<input type="text" placeholder="Entrez votre numéro de tel" name="noPhone" required >
				 <span class="erreur">${erreurs['noPhone']}</span>
                <br>


				<label for="usurname"><b>Mail : </b></label>
				<input type="text" placeholder="Entrez votre mail" name="mail">
				 <span class="erreur">${erreurs['mail']}</span>
                <br />
				 <p class="erreur">${resultat}</p>

				<button type="submit">Créer compte</button>

			</div>


		</form>

		<form action="Connexion" method="get">
			<button type="submit">Se Connecter</button>
		</form>
</div>
 <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>    
</body>
</html>
