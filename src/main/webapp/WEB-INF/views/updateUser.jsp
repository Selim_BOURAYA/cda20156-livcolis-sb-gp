<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link href="${pageContext.request.contextPath}/resources/style/style.css" rel="stylesheet" type="text/css">
<link href="${pageContext.request.contextPath}/resources/css/style.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<div class="position-relative p-3 p-md-5 m-md-3 text-center">


	<h1>Bienvenue ${sessionScope.transporteur.nom}</h1>
	<br>
	
	<h2>Modifier un client : veuillez modifier les champs souhaités puis valider</h2>

	<form action="${pageContext.request.contextPath}/updateClient" method="post" required>
	<br>Numéro de client : <input type="text" value="${client.id }" name="id" readonly="readonly" required>
	<br>prénom : <input type="text" placeholder="${client.prenom }" name="prenom" required>
	<br>nom : <input type="text" placeholder="${client.nom }" name="nom" required>
	<br>mail : <input type="text" placeholder="${client.mail }" name="mail" required>
	<br>téléphone : <input type="text" placeholder="${client.telephone }" name="telephone" required><br><br>
	<button type="submit">Valider les modifications</button>
	</form>
	</div>
	
	    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>    
</body>
</html>
