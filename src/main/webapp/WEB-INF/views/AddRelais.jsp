<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link href="${pageContext.request.contextPath}/resources/css/style.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<title>Ajout Relais</title>
</head>
<body>


  <div class="main_nav" center>
      <nav class="navbar fixed-top navbar-expand-lg navbar-light" style="background-color: #C0D1E8;" >
             <a class="navbar-brand" href="#">Liv colis</a>
         <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
           <span class="navbar-toggler-icon"></span>
         </button>

         <div class="collapse navbar-collapse" id="navbarSupportedContent" >
           <ul class="navbar-nav mr-auto">
             <li class="nav-item active">
               <a href="<c:out value ="${pageContext.request.contextPath}/Deconnexion"/>">Se déconnecter</a>
             </li>
          </ul>



         </div>
       </nav>

       <br>
         <br>
  </div>

  <br><br>
<div class="position-relative p-3 p-md-5 m-md-3 text-center">

			<h1>Ajout relais</h1>

		<div>
        <form action="addRelais" method="post">
			<div class = "container">
				<label for="uname"><b>Entrez adresse : </b></label>
				<input type="text" name="adresse" required><br>

				<label for="usurname"><b>Entrez Ville: </b></label>
				<input type="text" name="ville"><br>


				<label for="uname"><b>Entrez Pays : </b></label>
				<input type="text" name="pays" required ><br>


				<label for="uname"><b>Entrez telephone : </b></label>
				<input type="text" name="telephone" required ><br>

				<label for="uname"><b>Entrez code : </b></label>
				<input type="text" name="code" required ><br>

				 <label for="uname"><b>Horaire d'ouverture (format "hh:mm") </b></label>
				<input type="text" name="horraireouvert" required ><br>

				 <label for="uname"><b>Horaire de fermeture (format "hh:mm")</b></label>
				<input type="text" name="horraireferme" required ><br>
                <br>



				<button type="submit">Ajouter Relais</button>

			</div>
			</form>
		</div>
		
		 <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>    
</body>

</html>
