<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
 
<link href="${pageContext.request.contextPath}/resources/style/style.css" rel="stylesheet" type="text/css">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">

</head>
<body>
  <div class="main_nav" center>
      <nav class="navbar fixed-top navbar-expand-lg navbar-light" style="background-color: #C0D1E8;" >
             <a class="navbar-brand" href="#">Liv colis</a>
         <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
           <span class="navbar-toggler-icon"></span>
         </button>

         <div class="collapse navbar-collapse" id="navbarSupportedContent" >
           <ul class="navbar-nav mr-auto">
             <li class="nav-item active">
               <a href="<c:out value ="${pageContext.request.contextPath}/Deconnexion"/>">Se déconnecter</a>
             </li>
          </ul>



         </div>
       </nav>

       <br>
         <br>
  </div>

  <br><br>
  <div class="position-relative p-3 p-md-5 m-md-3 text-center">


  <div class="main_nav" center>
      <nav class="navbar fixed-top navbar-expand-lg navbar-light bg-light" >
             <a class="navbar-brand" href="#">Liv colis</a>
         <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
           <span class="navbar-toggler-icon"></span>
         </button>

         <div class="collapse navbar-collapse" id="navbarSupportedContent" >
           <ul class="navbar-nav mr-auto">
             <li class="nav-item active">
               <a href="<c:out value ="${pageContext.request.contextPath}/Deconnexion"/>">Se déconnecter</a>
             </li>
          </ul>



         </div>
       </nav>

       <br>
         <br>
  </div>


		<h1>Ajout colis</h1>
        <form action="addcolis" method="post">
			<div class = "container">
				<label for="uname"><b>Entrez Poids : </b></label>
				<input type="text" name="poids" required><br>

				<label for="usurname"><b>Entrez Emetteur : </b></label>
				<input type="text" name="idEmetteur"><br>


				<label for="uname"><b>Entrez Destinataire : </b></label>
				<input type="text" name="idDestinataire" required >

                <br>




				<!-- <label for="usurname"><b>Statut de colis : </b></label>
				<input type="text" name="statut"><br> -->


				<label for="country">Relais de départ :</label>
					<c:set var="i" value="0" />
                <select name="choixRelaisfirst" class="custom-select d-block w-100" id="relais">

               	 <c:forEach items="${listeRelais}" var="relais">


                	  <option value = '<c:out value="${relais.id}" />'><c:out value="${relais.adresse}" /> &nbsp; <c:out value="${relais.ville}" /></option>


				</c:forEach>
                </select>

               		<br>
               		<label for="country">Relais intermediaire :</label>
					<c:set var="i" value="0" />
                <select name="choixRelais" class="custom-select d-block w-100" id="relais">

               	 <c:forEach items="${listeRelais}" var="relais">


                	  <option value = '<c:out value="${relais.id}" />'><c:out value="${relais.adresse}" /> &nbsp; <c:out value="${relais.ville}" /></option>


				</c:forEach>
                </select>

					<br>
					<label for="country">Points arrivé :</label>
					<c:set var="i" value="0" />
                <select name="choixRelaisfin" class="custom-select d-block w-100" id="relais">

               	 <c:forEach items="${listeRelais}" var="relais">


                	  <option value = '<c:out value="${relais.id}" />'><c:out value="${relais.adresse}" /> &nbsp; <c:out value="${relais.ville}" /></option>


				</c:forEach>
                </select>

				<button type="submit">Ajouter colis</button>

			</div>




	</form>


	<br>
  <div class="position-relative p-3 p-md-5 m-md-3 text-center">
	<p>Si il n'y a pas votre point relais vous pouvez l'ajouter ici  : <a href="${pageContext.request.contextPath}/addRelais">
        <button type="button" name="retour accueil" value="retour accueil">Ajouter un point relais</button></a></p>


	<br>
		<a href="${pageContext.request.contextPath}/espaceTranspo">
        <button type="button" name="retour accueil" value="retour accueil">Retour à l'accueil</button></a>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>    
</body>
</body>
</html>
