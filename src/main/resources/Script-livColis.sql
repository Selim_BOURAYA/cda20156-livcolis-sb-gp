----------------------------------------------------------------------
--INITIALISATION DES CLIENTS ... PLEASE CHILL AND WAIT FEW SECONDS....
----------------------------------------------------------------------

insert into client 
values 
(nextval('client_seq'), 'bark@mail.fr','Pyke', 'SAO','0760069012'),
(nextval('client_seq'), 'sim@mail.fr','Dan', 'SER','0662269015'),
(nextval('client_seq'), 'test@mail.fr','Dams', 'BOU','0798579073'),
(nextval('client_seq'), 'smile@mail.fr','Mams', 'SER','0798579073'),
(nextval('client_seq'), 'tron@mail.fr','Dads','BOU','0645349012');


------------------------------------------------
--INSERTION DE COLIS
------------------------------------------------
insert into colis
values
(nextval('colis_seq'), '1.0',1,5),
(nextval('colis_seq'),'2.3',1,5),
(nextval('colis_seq'), '1.8',1,5),
(nextval('colis_seq'),'1.6',2,3),
(nextval('colis_seq'),'2.1',3,1);

------------------------------------------------
--INSERTION DE COMPTE
------------------------------------------------
insert into compte
values
(nextval('compte_seq'), 'poste','123'),
(nextval('compte_seq'), 'demeco','123'),
(nextval('compte_seq'), 'logic','123'),
(nextval('compte_seq'), 'prevote','123'),
(nextval('compte_seq'), 'ups','123');


------------------------------------------------
--INSERTION DE TRANSPORTEUR
------------------------------------------------
insert into transporteur
values 
(nextval('transport_seq'),'poste@mail.fr','La Poste', 'poste',  '0760069012',1 ),
(nextval('transport_seq'), 'demeco@mail.fr','DEMECO', 'demeco','0662269015',2 ),
(nextval('transport_seq'), 'logilow@mail.fr','LogiLow','logic','0798579073',3),
(nextval('transport_seq'), 'prevote@mail.fr','Prévoté','prevote','0798579073',4),
(nextval('transport_seq'), 'ups@mail.fr','UPS', 'ups','0645349012',5);


------------------------------------------------
--INSERTION DE RELAIS
------------------------------------------------
insert into relais
values 
(nextval('relais_seq'),'11 rue Republique', '45000','10:00:00','19:00:00','FRANCE', '0760069012','ORLEANS'),
(nextval('relais_seq'), '1 rue Liberte', '59000','10:00:00','18:00:00','FRANCE','0662269015','LILLE'),
(nextval('relais_seq'), '9 rue Paul Vaillant Couturier','80000','10:00:00','18:00:00','FRANCE','0798579073','AMIENS'),
(nextval('relais_seq'), '12 rue Berlioz','75000','10:00:00','18:30:00','FRANCE','0798579073','PARIS'),
(nextval('relais_seq'), '7 rue des acacias','69000','10:00:00','19:00:00','BEAUVAIS','FRANCE','LYON');

----------------------------------------------------
--INSERTION DE LIVRAISONS (id colis, id transporteur
----------------------------------------------------

insert into livraison
values 
(nextval('livraison_seq'),'10000001',1,3), 
(nextval('livraison_seq'),'10000002',2,3), 
(nextval('livraison_seq'),'10000003',3,3), 
(nextval('livraison_seq'),'10000004',4,4), 
(nextval('livraison_seq'),'10000005',5,5); 

-------------------------------------------------------------
--INSERTION DE LISTERELAISPARCOURUS (id livraison, id relais)
-------------------------------------------------------------

insert into listerelaisparcourus
values 
(1,1),
(1,2),
(1,3),
(2,4),
(2,5),
(3,1),
(3,5),
(4,1),
(4,2),
(4,3),
(4,4),
(5,3),
(5,4),
(5,5);


-------------------------------------------------------------
--INSERTION DE HEURELIVRAISON (id livraison, id relais)
-------------------------------------------------------------

insert into heurelivraison
values 
(nextval('heure_seq'),'25-11-2020 12:43:39','colis pris en charge',1),
(nextval('heure_seq'),'26-11-2020 15:30:39','statut 2',1),
(nextval('heure_seq'),'28-11-2020 17:43:39','colis livré',1),
(nextval('heure_seq'),'25-11-2020 12:43:39','colis pris en charge',2),
(nextval('heure_seq'),'26-11-2020 15:30:39','statut 2',2),
(nextval('heure_seq'),'28-11-2020 17:43:39','colis livré',2),
(nextval('heure_seq'),'25-11-2020 12:43:39','colis pris en charge',3),
(nextval('heure_seq'),'26-11-2020 15:30:39','statut 2',3),
(nextval('heure_seq'),'28-11-2020 17:43:39','colis livré',3),
(nextval('heure_seq'),'25-11-2020 12:43:39','colis pris en charge',4),
(nextval('heure_seq'),'26-11-2020 15:30:39','statut 2',4),
(nextval('heure_seq'),'28-11-2020 17:43:39','statut 3',4),
(nextval('heure_seq'),'25-11-2020 12:43:39','colis livré',4),
(nextval('heure_seq'),'26-11-2020 15:30:39','colis pris en charge',5),
(nextval('heure_seq'),'28-11-2020 17:43:39','statut 2',5),
(nextval('heure_seq'),'30-11-2020 12:43:39','colis livré',5);

