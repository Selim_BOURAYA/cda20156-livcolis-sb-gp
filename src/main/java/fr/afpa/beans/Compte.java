package fr.afpa.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString

@Entity (name="compte")
public class Compte {

	
	@Id @Column(name = "compte_id")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "compte_generator")
	@SequenceGenerator(name="compte_generator", sequenceName = "compte_seq", initialValue = 1, allocationSize = 1)
	private Long id;
	
	@Column(name = "login", unique = true)
	private String login;
	
	@Column(name = "password")
	private String password;

	public Compte (String login, String password) {
		
		this.login = login;
		this.password = password;
	}
}
