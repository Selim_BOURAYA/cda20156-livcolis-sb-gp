package fr.afpa.beans;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString

@Entity (name="transporteur")
public class Transporteur {
	
	@Id @Column(name = "transporteur_id")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "transport_generator")
	@SequenceGenerator(name="transport_generator", sequenceName = "transport_seq", initialValue = 1, allocationSize = 1)
	private Long id;
	
	@OneToOne
	@JoinColumn(referencedColumnName = "compte_id")
	private Compte compte;
		
	@Column(name = "prenom")
	private String prenom;
	
	@Column(name = "nom")
	private String nom;
	
	@Column(name = "mail", unique = true)
	private String mail;
	
	@Column(name = "telephone")
	private String telephone;
	
	@Column(name = "listeLivraisonTransporteur")
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "transporteur")
	private List <Livraison> listeLivraison;

	
	public Transporteur(String prenom, String nom, String mail, String telephone, Compte compte) {
		
		this.prenom = prenom;
		this.nom = nom;
		this.mail = mail;
		this.telephone = telephone;
		this.compte = compte;
		
	}
	
	public Transporteur(Long id, String prenom, String nom, String mail, String telephone, Compte compte) {
		
		this.id = id;
		this.prenom = prenom;
		this.nom = nom;
		this.mail = mail;
		this.telephone = telephone;
		this.compte = compte;
		
	}

}


