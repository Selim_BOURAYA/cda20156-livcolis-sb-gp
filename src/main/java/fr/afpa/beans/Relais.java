package fr.afpa.beans;

import java.time.LocalTime;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString

@Entity (name="relais")
public class Relais {
	
	@Id @Column(name = "relais_id")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "relais_generator")
	@SequenceGenerator(name="relais_generator", sequenceName = "relais_seq", initialValue = 1, allocationSize = 1)
	private Long id;
	
	@Column(name = "adresse")
	private String adresse;
	
	@Column(name = "code")
	private String code;
	
	@Column(name = "ville")
	private String ville;
	
	@Column(name = "pays")
	private String pays;
	
	@Column(name = "telephone")
	private String telephone;
	
	@Column(name = "ouverture")
	private LocalTime ouverture;
	
	@Column(name = "fermeture")
	private LocalTime fermeture;
	
	@ManyToMany (mappedBy = "listeRelais")
	private List <Livraison> listeLivraison;
	
	
	public Relais(String adresse, String code, String ville, String pays, String telephone, LocalTime ouverture, LocalTime fermeture) {
		
		this.adresse = adresse ;
		this.code = code; 
		this.ville = ville;
		this.pays = pays; 
		this.telephone = telephone; 
		this.ouverture = ouverture;
		this.fermeture = fermeture;
	}


	public Relais(Long id, String adresse, String code, String ville, String pays, String telephone,
			LocalTime ouverture, LocalTime fermeture) {
		super();
		this.id = id;
		this.adresse = adresse;
		this.code = code;
		this.ville = ville;
		this.pays = pays;
		this.telephone = telephone;
		this.ouverture = ouverture;
		this.fermeture = fermeture;
	}
	
	
}
