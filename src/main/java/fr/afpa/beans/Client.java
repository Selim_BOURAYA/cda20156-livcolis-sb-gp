package fr.afpa.beans;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString

@Entity (name="client")
public class Client {

	@Id @Column(name = "client_id")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "client_generator")
	@SequenceGenerator(name="client_generator", sequenceName = "client_seq", initialValue = 1, allocationSize = 1)
	private Long id;
	
	@Column(name = "prenom")
	private String prenom;
	
	@Column(name = "nom")
	private String nom;
	
	@Column(name = "mail")
	private String mail;
	
	@Column(name = "telephone")
	private String telephone;
	
	@Column(name = "listeColisEmetteur")
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "emetteur")
	private Set <Colis> listeColisEmetteur = new HashSet <Colis>();
	
	@Column(name = "listeColisDestinataire")
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "destinataire")
	private Set  <Colis> listeColisDestinataire = new HashSet <Colis>();
	
	
	public Client(Long id, String prenom, String nom, String mail, String telephone) {
		
		this.id = id;
		this.prenom = prenom;
		this.nom = nom;
		this.mail = mail;
		this.telephone = telephone;
		
	}
	
	public Client(String prenom, String nom, String mail, String telephone) {
		
		this.prenom = prenom;
		this.nom = nom;
		this.mail = mail;
		this.telephone = telephone;
		
	}
	
	public Client(int id, String nom) {
		
		this.id = (long) id;
		this.nom = nom;
				
	}
	
}
