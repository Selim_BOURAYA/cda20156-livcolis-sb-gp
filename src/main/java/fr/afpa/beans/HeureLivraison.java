package fr.afpa.beans;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import fr.afpa.utils.Utilitaires;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor

@Entity (name="heurelivraison")
public class HeureLivraison {
	

		@Id @Column(name = "heurelivraison_id")
		@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "heure_generator")
		@SequenceGenerator(name="heure_generator", sequenceName = "heure_seq", initialValue = 1, allocationSize = 1)
		private Long id;
		
		@Column(name = "heurelivraison" )
		private String dateLivraison;
		
		@Column(name = "statut" )
		private String statut;
		
		@ManyToOne (cascade = CascadeType.MERGE)
		@JoinColumn (name = "fk_livraison", referencedColumnName = "livraison_id")
		private Livraison livraison;
		
		
		public HeureLivraison() {
			
			dateLivraison = Utilitaires.addHeure();
		}
		
		public HeureLivraison(String statut) {
			
			this.statut = statut;
			dateLivraison = Utilitaires.addHeure();
		}

		public HeureLivraison(String statut, Livraison livraison) {
			super();
			this.statut = statut;
			this.livraison = livraison;
			dateLivraison = Utilitaires.addHeure();
		}

		
		
}
