package fr.afpa.beans;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString

@Entity (name="colis")
public class Colis {

	@Id @Column(name = "colis_id")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "colis_generator")
	@SequenceGenerator(name="colis_generator", sequenceName = "colis_seq", initialValue = 1, allocationSize = 1)
	private Long id;
	
	@Column(name = "poids")
	private float poids;
		
	@OnDelete(action = OnDeleteAction.CASCADE)
	@ManyToOne (cascade = CascadeType.MERGE)
	@JoinColumn (name = "fk_emetteur", referencedColumnName = "client_id")
	private Client emetteur;
	
	
	@OnDelete(action = OnDeleteAction.CASCADE)
	@ManyToOne (cascade = CascadeType.MERGE)
	@JoinColumn (name = "fk_destinataire", referencedColumnName = "client_id")
	private Client destinataire;
	
	
	public Colis(float poids) {
		
		this.poids = poids;
	}
	
	public Colis(int id, float poids) {
		
		this.id = (long)id;
		this.poids = poids;
	}

	public Colis(float poids, Client emetteur, Client destinataire) {
		super();
		this.poids = poids;
		this.emetteur = emetteur;
		this.destinataire = destinataire;
	}
	
}
