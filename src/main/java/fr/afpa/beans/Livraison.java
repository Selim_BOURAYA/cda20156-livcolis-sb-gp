package fr.afpa.beans;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString

@Entity (name="livraison")

public class Livraison {
	
	@Id @Column(name = "livraison_id")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "livraison_generator")
	@SequenceGenerator(name="livraison_generator", sequenceName = "livraison_seq", initialValue = 1, allocationSize = 1)
	private Long id;
	
	@OneToOne
	@JoinColumn(name = "colis_id" )
	private Colis colis;
	
	@Column(name="listeHeureReception")
	@OneToMany(cascade = CascadeType.MERGE, fetch = FetchType.EAGER, 
				mappedBy = "livraison")
	private Set <HeureLivraison> listeHeureReception;
	
	@ManyToOne (cascade = CascadeType.MERGE)
	@JoinColumn (name = "fk_transporteur", referencedColumnName = "transporteur_id")
	private Transporteur transporteur;
	
	@ManyToMany(cascade = { CascadeType.MERGE }, fetch = FetchType.EAGER)
	    @JoinTable(
	        name = "listeRelaisParcourus", 
	        joinColumns = { @JoinColumn(name = "livraison_id") }, 
	        inverseJoinColumns = { @JoinColumn(name = "relais_id") }
	    )
	private Set <Relais> listeRelais;
	
	@Column(name="codeclient")
	private String codeClient;
	
	

	public Livraison(Colis colis) {
		
		this.colis = colis;
	}

	public Livraison(Transporteur transporteur) {
		
		this.transporteur = transporteur;
	}

}
