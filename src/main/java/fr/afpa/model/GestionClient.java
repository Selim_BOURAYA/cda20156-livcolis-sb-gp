package fr.afpa.model;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.beans.Client;
import fr.afpa.beans.Colis;
import fr.afpa.beans.Livraison;
import fr.afpa.constants.Constants;
import fr.afpa.repository.dao.ClientRepository;
import fr.afpa.repository.dao.ColisRepository;
import fr.afpa.repository.dao.LivraisonRepository;
import lombok.Getter;

@Getter
@Service
public class GestionClient implements Constants {

	@Autowired
	private ClientRepository clientRepository;
	@Autowired
	private LivraisonRepository livraisonRepository3;
	@Autowired
	private ColisRepository colisRepository3;
	private Client client;
	private Livraison livraisonClient;
	private ArrayList<Client> listeClient;
	private ArrayList<Colis> listeColisClient;
	private ArrayList<Colis> listeColisSupp;
	private ArrayList<Livraison> listeLivraison;
	private ArrayList<Livraison> listeLivraisonSupp;
	private Long idClientLong;

	/**
	 * @author Selim méthode d'ajout du client dans la base de donnée. Renvoi d'une
	 *         String de confirmation de l'ajout
	 */
	public String addClient(String prenom, String nom, String mail, String telephone) {

		client = new Client(prenom, nom, mail, telephone);
		clientRepository.save(client);

		return viewlisteClient;
	}

	/**
	 * @author Selim méthode de recensement des clients inscrits dans la base de
	 *         donnée. Renvoi une ArrayList de client
	 */
	public ArrayList<Client> listClient() {

		listeClient = (ArrayList<Client>) clientRepository.findAll();

		return listeClient;
	}

	/**
	 * @author Selim méthode de récupération d'une entité transporteur, à partir de
	 *         son id. Les informations du transporteur correspondant, enregistrées
	 *         dans la BDD sont renvoyé et enregistré dans un objet
	 * @return
	 */
	public Client getClient(String idClient) {

		idClientLong = Long.parseLong(idClient);
		Optional<Client> clientBDD = clientRepository.findById(idClientLong);

		if (clientBDD != null) {

			client = clientBDD.get();
			return client;
		}
		return null;
	}

	/**
	 * @author Selim méthode de récupération de la livraison client, à partir de son
	 *         du code Client. Les informations du transporteur correspondant,
	 *         enregistrées dans la BDD sont renvoyées et enregistrées dans un objet
	 *         qui est renvoyé vers la couche controlleur
	 * @return
	 */
	public Livraison getLivraisonClient(String codeClient) {

		livraisonClient = livraisonRepository3.findByCodeClient(codeClient);

		if (livraisonClient != null) {
			return livraisonClient;
		}
		return null;
	}

	/**
	 * @author Selim méthode de modification des informations clients à partir des
	 *         nouvelles informations enregistrées dans le formulaire de correction.
	 *         Renvoi une String de validation de la modification (ajout de contrôle
	 *         sans doute nécessaires)
	 */
	public String modifClient(String id, String prenom, String nom, String mail, String telephone) {

		client = getClient(id);

		client.setPrenom(prenom);
		client.setNom(nom);
		client.setMail(mail);
		client.setTelephone(telephone);

		clientRepository.saveAndFlush(client);

		return viewlisteClient;

	}

	/**
	 * @author Selim méthode de suppression d'un client dans la base de donnée.
	 *         Renvoi une String contenant le nom de la prochaine vue correction
	 *         nécessaire dans la couche bean entity
	 */
	public String suppClient(String id) {

		idClientLong = Long.parseLong(id);
		Optional<Client> clientBDD = clientRepository.findById(idClientLong);

		if (clientBDD != null) {

			listeColisClient = new ArrayList<Colis>();
			listeColisClient.addAll(clientBDD.get().getListeColisDestinataire());
			listeColisClient.addAll(clientBDD.get().getListeColisEmetteur());

			listeLivraison = (ArrayList<Livraison>) livraisonRepository3.findAll();
			listeLivraisonSupp = new ArrayList<Livraison>();

			for (Colis colis : listeColisClient) {
				for (Livraison livraison : listeLivraison) {

					if (colis.getId() == livraison.getColis().getId()) {

						listeLivraisonSupp.add(livraison);

					}
				}
			}

			colisRepository3.deleteAll(listeColisClient);
			colisRepository3.flush();
			livraisonRepository3.deleteAll(listeLivraisonSupp);
			livraisonRepository3.flush();
			clientRepository.deleteById(idClientLong);
			clientRepository.flush();

			return viewlisteClient;

		}

		return viewlisteClient;
	}

}
