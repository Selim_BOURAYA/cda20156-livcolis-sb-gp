package fr.afpa.model;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.beans.Relais;
import fr.afpa.repository.dao.RelaisRepository;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Service
public class GestionRelais {

	@Autowired
	private RelaisRepository relaisRepository;
	private Relais relais;
	private ArrayList<Relais> listeRelais;
	private int idRelais;

	/**
	 * @author Selim méthode d'ajout d'un relais dans la base de donnée. Renvoi
	 *         d'une String de confirmation de l'ajout
	 */
	public String addRelais(String adresse, String code, String ville, String pays, String telephone,
			LocalTime ouverture, LocalTime fermeture) {

		relais = new Relais(adresse, code, ville, pays, telephone, ouverture, fermeture);
		relaisRepository.save(relais);

		return "constante relais ajouté";
	}

	/**
	 * @author Selim méthode de recensement des relais inscrits dans la base de
	 *         donnée. Renvoi une ArrayList de relais
	 */
	public ArrayList<Relais> listRelais() {
		listeRelais = new ArrayList<Relais>();
		listeRelais = (ArrayList<Relais>) relaisRepository.findAll();

		return listeRelais;
	}

	/**
	 * @author Selim  & Greg  : 
	 * 			méthode de modification des informations relais à partir des
	 *         nouvelles informations enregistrées dans le formulaire de correction.
	 *         Renvoi une String de validation de la modification (ajout de contrôle
	 *         sans doute nécessaires)
	 */
	public String modifRelais(String id, String adresse, String code, String ville, 
			String pays, String telephone, LocalTime ouverture, LocalTime fermeture) {

		idRelais = Integer.parseInt(id);
		
		/*Optional<Relais> relais = relaisRepository.findById((long) idRelais);
		
		relais.get().setAdresse(adresse);
		relais.get().setCode(code);
		relais.get().setVille(ville);
		relais.get().setPays(pays);
		relais.get().setTelephone(telephone);
		relais.get().setOuverture(ouverture);
		relais.get().setFermeture(fermeture);*/
		//relaisRepository.saveAndFlush(relais.get());
		relais = new Relais((long) idRelais, adresse, code, ville, pays, telephone, ouverture, fermeture);
		
		relaisRepository.saveAndFlush(relais);
		
		
		

		return "constante relais modifié";
	}

	/**
	 * @author Selim méthode de suppression d'un relais dans la base de donnée.
	 *         Renvoi une String de confirmation
	 */
	public String suppRelais(String id) {

		idRelais = Integer.parseInt(id);
		relaisRepository.deleteById((long) idRelais);
		relaisRepository.flush();

		return "constante relais supprimé";
	}

	public Relais getRelais(String id) {
		// TODO Auto-generated method stub
		idRelais = Integer.parseInt(id);
		
		Optional <Relais >relais1 = relaisRepository.findById((long) idRelais);
		return relais1.get();
	}

}