package fr.afpa.model;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.beans.Client;
import fr.afpa.beans.Colis;
import fr.afpa.repository.dao.ClientRepository;
import fr.afpa.repository.dao.ColisRepository;
import fr.afpa.utils.Utilitaires;
import lombok.Getter;
@Getter
@Service
public class GestionColis implements Utilitaires  {

	@Autowired
	private ColisRepository colisRepository;
	@Autowired
	private ClientRepository clientRepository2;
	@Autowired
	private GestionClient gestionClient2;
	private Client emetteur;
	private Client destinataire;
	private Colis colis; 
	private Client client; 
	private Client clientBDD; 
	private ArrayList<Colis> listeColis;
	private int sizeList;
	private Long idColis;
	private Long racineCode;
	private String code = "";
	
	
	
	/**
	 * @author Selim
	 * méthode de création du colis. L'id des emetteur et destinataire sont récupérée en entrée, en plus du poids.
	 * La méthode renvoi un colis et sera appelée par la méthode "buildLivraison()" depuis la GestionTransporteur
	 * @return
	 */
	public boolean addColis(String idEmetteur, String idDestinataire, String poids) {
		
		Optional<Client> emetteurBDD = clientRepository2.findById(Long.parseLong(idEmetteur));
		Optional<Client> destinataireBDD = clientRepository2.findById(Long.parseLong(idDestinataire));
				
		if(emetteurBDD != null && destinataireBDD != null && emetteurBDD != destinataireBDD) {
			
			emetteur = emetteurBDD.get();
			destinataire = destinataireBDD.get();
			
			colis = new Colis(Float.parseFloat(poids));
			colis.setEmetteur(emetteur);
			colis.setDestinataire(destinataire);
			colisRepository.save(colis);
			
			return true;
		}
		
		return false;
	}
	
	/*
	 * 
	 * On renvoi au controller un colis pour créer la livraison
	 * 
	 * */
		public Colis addColisController(String idEmetteur, String idDestinataire, String poids) {
		
		Optional<Client> emetteurBDD = clientRepository2.findById(Long.parseLong(idEmetteur));
		Optional<Client> destinataireBDD = clientRepository2.findById(Long.parseLong(idDestinataire));
				
		if(emetteurBDD != null && destinataireBDD != null && emetteurBDD != destinataireBDD) {
			
			emetteur = emetteurBDD.get();
			destinataire = destinataireBDD.get();
			
			colis = new Colis(Float.parseFloat(poids));
			colis.setEmetteur(emetteur);
			colis.setDestinataire(destinataire);
			colisRepository.save(colis);
			
			return colis;
		}
		return colis;
		
		
	}
	
	/**
	 * @author Selim
	 * méthode de récupérarion d'un objet colis, à partir de son id. Les informations du colis correspondant, enregistrées
	 *  dans la BDD. La méthode renvoie un colis et sera appelée par la méthode "buildLivraison()" depuis la GestionTransporteur
	 * @return
	 */
	public Colis getColis(String id) {
		
		Optional <Colis >colisBDD = colisRepository.findById(Long.parseLong(id));
		
		if(colisBDD != null) {
		colis = colisBDD.get();
		return colis;
		}
		return null;
	}
	
	
	/**
	 * @author Selim
	 * méthode de récupération d'un objet List de colis, à partir de l'id de l'emtteur. On récupére la dernière entrée de la liste 
	 * et les informations du colis correspondant, enregistrées dans la BDD. 
	 * La méthode renvoie le dernier colis enregistré par l'emetteur et sera appelée par la méthode "buildLivraison()" 
	 * depuis la GestionTransporteur
	 * @return
	 */
	public Colis getColisEmetteur(String idEmetteur) {

		Optional<Client> emetteurBDD = clientRepository2.findById(Long.parseLong(idEmetteur));

		if (emetteurBDD != null) {

			clientBDD = emetteurBDD.get();
			listeColis = colisRepository.findByEmetteur(clientBDD);
			
			if (listeColis != null) {
				
				listeColis.stream().sorted(Comparator.comparing(Colis::getId));
				sizeList = listeColis.size();
				colis = listeColis.get(sizeList-1);
								
				return colis;
			}
			return null;
		}
		return null;
	}
	
	/**
	 * @author Selim
	 * méthode de recensement des colis présents dans la base de données
	 * @return
	 */
	public ArrayList <Colis>listColis() {
			
		listeColis = (ArrayList<Colis>) colisRepository.findAll();
		
		return listeColis;
	}
	
	/**
	 * @author Selim
	 * méthode de suppression d'un colis dans la base de données
	 * @return
	 */
	public void suppColis(String id) {
				
		idColis = Long.parseLong(id);
		colisRepository.deleteById(idColis);
		colisRepository.flush();
	}
	
	/**
	 * @author Selim
	 * méthode de création d'un code de livraison, qui va concaténer un Long aléatoire au code colis
	 * Ce code sera transmis par la suite à l'emetteur et au destinataire
	 * @return
	 */
	public String creationCode(Colis colis) {
		
		racineCode = getRandomLong(100000,999998); 
		code = racineCode+""+colis.getId(); 
		return code;	 	
		
	}
		
	
	public void envoiCodeClient(Colis colis) {
		
		
			
	}
	
	
}
