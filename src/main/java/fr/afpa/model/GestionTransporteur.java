package fr.afpa.model;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Properties;
import java.util.Set;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.ModelAndView;

import fr.afpa.beans.Colis;
import fr.afpa.beans.Compte;
import fr.afpa.beans.HeureLivraison;
import fr.afpa.beans.Livraison;
import fr.afpa.beans.Relais;
import fr.afpa.beans.Transporteur;
import fr.afpa.constants.Constants;
import fr.afpa.repository.dao.CompteRepository;
import fr.afpa.repository.dao.HeureLivRepository;
import fr.afpa.repository.dao.LivraisonRepository;
import fr.afpa.repository.dao.TransporteurRepository;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Service
public class GestionTransporteur implements Constants {

	@Autowired
	private TransporteurRepository transporteurRepository;
	@Autowired
	private CompteRepository compteRepository;
	@Autowired
	private LivraisonRepository livraisonRepository;
	@Autowired
	private GestionColis gestionColis;

	@Autowired
	private HeureLivRepository heurelivrepository;

	@Autowired
	private HeureLivRepository heurelivraisonRepository;

	private Transporteur transporteur;
	private Transporteur transporteurAuth;
	private Compte compte;
	private Colis colis;
	private Livraison livraison;
	private List<Livraison> listeLivraison;
	private int idLivraison;
	private HashSet<Relais> relaisList = new HashSet<Relais>();
	private int nombreRelaisTotal;
	private int nombreRelaisParcourus;
	private String view;
	private String codeClient;
	private HeureLivraison heureLivraison;
	private ModelAndView mv;

	/**
	 * @author Gregory méthode d'ajout d'un transporteur et de son compte . Renvoi
	 *         le transporteur à ajouter en BDD
	 */
	public String addTransporteur(String prenom, String nom, String mail, String telephone, String login,
			String password) {

		compte = new Compte(login, password);
		compteRepository.save(compte);
		transporteur = new Transporteur(prenom, nom, mail, telephone, compte);
		transporteurRepository.save(transporteur);

		return "ajout réussi";
	}

	/**
	 * @author Gregory connexion transporteur :
	 * 
	 *         On rentre en parametre le login et le mot de passe et on va chercher
	 *         dans la BDD le compte correspondant au transporteur qu'on renvoi au
	 *         controlleur (pas besoin de renvoyer l'objet)
	 */
	public String authTransporteur(String login, String password) {

		compte = compteRepository.findByLoginAndPassword(login, password);

		if (compte != null) {

			transporteur = transporteurRepository.findByCompte(compte);
			// on enregistre les informations du transporteur connecté dans une variable
			// locale
			transporteurAuth = transporteur;

			return "auth reussi";
		}
		return "auth echouee";

	}

	/**
	 * @author Selim méthode de récupération d'une entité transporteur, à partir de
	 *         son id. Les informations du transporteur correspondant, enregistrées
	 *         dans la BDD sont renvoyé et enregistré dans un objet
	 * @return
	 */
	public Transporteur getTransporteur(String idTransporteur) {

		Optional<Transporteur> transporteurBDD = transporteurRepository.findById(Long.parseLong(idTransporteur));

		if (transporteurBDD != null) {

			transporteur = transporteurBDD.get();
			return transporteur;
		}
		return null;
	}

	/**
	 * @author Selim méthode de récupération d'une entité livraison, à partir de son
	 *         id. Les informations correspondantes, enregistrées dans la BDD sont
	 *         renvoyé et enregistré dans un objet
	 * @return
	 */
	public Livraison getLivraison(String idLivraison) {

		Optional<Livraison> livraisonBDD = livraisonRepository.findById(Long.parseLong(idLivraison));

		if (livraisonBDD != null) {

			livraison = livraisonBDD.get();
			return livraison;
		}
		return null;
	}

	/**
	 * @author Selim méthode d'instanciation de la livraison. La méthode fait appel
	 *         à "addColis()" afin de récupérer le colis qui est ensuite ajoutée à
	 *         la base de données en appelant la méthode "addLivraison()"
	 */
	public String buildLivraison(String idColis, Set<Relais> listeRelais) {

		colis = gestionColis.getColis(idColis);
		codeClient = gestionColis.creationCode(colis);

		livraison = new Livraison();
		livraison.setColis(colis);

		livraison.setListeRelais(listeRelais);
		livraison.setCodeClient(codeClient);
		livraison.setTransporteur(transporteurAuth);

		gestionColis.envoiCodeClient(colis);

		addLivraison(livraison);

		return "livraison enregistrée";
	}

	/**
	 * author Selim méthode récupérant la liste de livraison de l'utilisateur
	 * connecté
	 * 
	 * @param id
	 */
	public List<Livraison> listLivraison() {

		listeLivraison = livraisonRepository.findByTransporteur(transporteurAuth);

		return listeLivraison;
	}

	/**
	 * author Selim méthode d'ajout d'une livraison en entrée, qui est ajoutée à la
	 * base de données grâce au repository"
	 */
	public String addLivraison(Livraison livraison) {

		livraisonRepository.save(livraison);

		return "livraison ajoutée";

	}

	/**
	 *
	 * @author Greg et Selim On modifie le statut et enregistre l'heure courante de
	 *         la livraison en allant la chercher dans la BDD avec le colis concerné
	 * 
	 */
	public String modifStatut(Livraison livraison) {

		nombreRelaisTotal = livraison.getListeRelais().size()-1;
		nombreRelaisParcourus = livraison.getListeHeureReception().size();
		System.out.println("total : " + nombreRelaisTotal + " parcourus : " + nombreRelaisParcourus);

		if (nombreRelaisParcourus < nombreRelaisTotal) {

			livraison.getListeHeureReception().add(new HeureLivraison("Votre colis est pris en charge par "
					+ livraison.getTransporteur().getNom() + " est en cours d'acheminement", livraison));
			livraisonRepository.saveAndFlush(livraison);

		}

		else if (nombreRelaisParcourus == nombreRelaisTotal) {

			livraison.getListeHeureReception().add(new HeureLivraison("votre colis est livré", livraison));
			livraisonRepository.saveAndFlush(livraison);
			System.out.println("sauvegarde et sorti");

		}

		return null;
	}

	/**
	 *
	 * @author Greg
	 * 
	 *         Désactiver les commentaire à l'interieure de la méthode pour tester
	 *         dans le controleur
	 */
	public String envoiMail(Livraison livraison, Relais relais, String emeteur, String destinataire) {

		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "smtp-mail.outlook.com");
		props.put("mail.smtp.port", "587");
		Session session = Session.getInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {

				return new PasswordAuthentication("gregmpembe@hotmail.fr", "Sound123");
			}
		});

		try {
			// Etape 2 : Création de l'objet Message toutes les adresses d'envoi doivent
			// être valide pour que le mail s'envoi
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress("gregmpembe@hotmail.fr"));

			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(emeteur));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(destinataire));

			message.setSubject("Test email");
			message.setText("Bonjour, votre colis " + livraison.getCodeClient() + " est à  " + relais.getAdresse());
			// Etape 3 : Envoyer le message
			Transport.send(message);
			System.out.println("Message_envoye");
		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}

		return "mail envoyé";
	}

	/**
	 *
	 * @author Greg
	 * 
	 */

	public String addRelaisToLivraison(Relais relais, Colis colis) {

		// réinitialise la liste de relais à chaque nouvelle entrée, on perd la liste de
		// relais renseignée dans la livraision

		// List<Relais> relaisList = new ArrayList<Relais>();
		// livraison = new Livraison();
		// relaisList.add(relais);

		livraison = livraisonRepository.findByColis(colis);

		livraison.getListeRelais().add(relais);

		livraisonRepository.saveAndFlush(livraison);

		return " Point Relais ajoutée à la livraison ";
	}

	/**
	 *
	 * @author Greg
	 * 
	 */
	public String suppLivraison(String id) {

		long idL = Long.parseLong(id);
		livraisonRepository.deleteById(idL);
		livraisonRepository.flush();

		return "livraison supprimé";
	}

	/*
	 * 
	 * @author GReg *
	 */
	public String modifHeureLivraison(HeureLivraison heureLiv) {

		heurelivrepository.saveAndFlush(heureLiv);

		return "Statut modifié";
	}
}
