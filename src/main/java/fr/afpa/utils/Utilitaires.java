package fr.afpa.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

public interface Utilitaires {
			
	public default Long getRandomLong(long min, long max) {
		
		if (min >= max) {
			throw new IllegalArgumentException("max must be greater than min");
		}

		Random r = new Random();
		return (long) (r.nextInt(((int)max - (int)min) + 1) + (int)min);
	}
	
	
	public static String addHeure() {

		 Date date = Calendar.getInstance().getTime();
		 DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		 String strDate = dateFormat.format(date);
		
		return strDate;
	}
	

}
