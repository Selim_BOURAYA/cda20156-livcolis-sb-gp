package fr.afpa.repository.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import fr.afpa.beans.Relais;

public interface RelaisRepository extends JpaRepository <Relais, Long>  {

}
