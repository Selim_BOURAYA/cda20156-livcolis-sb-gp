package fr.afpa.repository.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.afpa.beans.HeureLivraison;

public interface HeureLivRepository extends JpaRepository<HeureLivraison,Long> {

}
