package fr.afpa.repository.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.afpa.beans.Client;

@Repository
public interface ClientRepository extends JpaRepository <Client, Long>{

	Client findByMail(String mail);
			
}
