package fr.afpa.repository.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.afpa.beans.Colis;
import fr.afpa.beans.Livraison;
import fr.afpa.beans.Transporteur;

@Repository
public interface LivraisonRepository extends JpaRepository <Livraison, Long>  {

	List <Livraison> findByTransporteur(Transporteur transporteur);

	Livraison findByColis(Colis colis);
	
	Livraison findByCodeClient(String codeClient);
}
