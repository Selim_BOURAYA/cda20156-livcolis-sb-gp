package fr.afpa.repository.dao;

import java.util.ArrayList;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.afpa.beans.Client;
import fr.afpa.beans.Colis;

@Repository
public interface ColisRepository extends JpaRepository <Colis, Long>{

	 ArrayList <Colis> findByEmetteur(Client client);
}


