package fr.afpa.repository.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.afpa.beans.Compte;
import fr.afpa.beans.Transporteur;

@Repository
public interface TransporteurRepository extends JpaRepository <Transporteur, Long> {

	Transporteur findByCompte(Compte compte);
}
