package fr.afpa.repository.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.afpa.beans.Compte;

@Repository
public interface CompteRepository extends JpaRepository <Compte, Long> {
	
	Compte findByLoginAndPassword(String login,String password);

}
