package fr.afpa.constants;

public interface Constants {
	
	public final String accueil = "accueil";
	
//PAGES TRANSPORTEUR
	
	public final String connexion = "Connexion";
	public final String creationCompte = "creationCompte";
	public final String espaceTransporteur = "espaceTransporteur";
	
//PAGES COLIS
	
	public final String menuColis = "menuColis";
	public final String addColis = "AddColis";
	public final String viewlisteColis = "listeColis";
	public final String parcoursColis = "parcoursColis";
	public final String parcoursColisClient = "parcoursColisClient";
	public final String gestionColis = "gestionColis";
	public final String updateColis = "updateColis";

	
//PAGES RELAIS
	
	public final String addRelais = "AddRelais";
	public final String listeRelais = "listeRelais";
	public final String updateRelais = "updateRelais";
	
	
//PAGES CLIENT
	
	public final String addUser = "AddUser";
	public final String viewlisteClient = "listeClient";
	public final String updateUser = "updateUser";
	














	
}
