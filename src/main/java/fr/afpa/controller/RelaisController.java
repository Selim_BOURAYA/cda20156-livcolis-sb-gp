package fr.afpa.controller;

import java.time.LocalTime;
import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import fr.afpa.beans.Livraison;
import fr.afpa.beans.Relais;
import fr.afpa.model.GestionRelais;
import fr.afpa.model.GestionTransporteur;

@Controller
public class RelaisController {
	
	
	@Autowired
	private GestionRelais gestionRelais;
	
	@Autowired
	private GestionTransporteur gestionTransporteurController;
	
	private ArrayList <Livraison> listeLivraison;
	
	private ArrayList<Relais> listeRelais;
	
	@GetMapping(value = {"/redirectionPageGestionRelais","deleteRelaisById/redirectionPageGestionRelais","updateRelais/redirectionPageGestionRelais"})
	public ModelAndView redirectionMenuRelais(ModelAndView mv) {
		
		listeRelais = gestionRelais.listRelais();
		
		mv.addObject("listeRelais", listeRelais);
		mv.setViewName("listeRelais");
		return mv;
	}

	@GetMapping(value = {"addRelais"})
	public ModelAndView AddRelaisDisplay(ModelAndView mv) {
		
		mv.setViewName("AddRelais");
		
	return mv;	
	}
	
	@PostMapping(value = {"addRelais"})
	public ModelAndView AddRelais(ModelAndView mv,
			@RequestParam(value = "adresse") String adresse,
			@RequestParam(value = "ville") String ville,
			@RequestParam(value = "telephone") String telephone,
			@RequestParam(value = "code") String code,
			@RequestParam(value = "pays") String pays,
			@RequestParam(value = "horraireouvert") String openhours,
			@RequestParam(value = "horraireferme") String closedhours) {
		
			LocalTime horraireouvert =  LocalTime.parse(openhours);
			LocalTime horraireferme =  LocalTime.parse(closedhours);
			
			gestionRelais.addRelais(adresse, code, ville, pays, telephone, horraireouvert, horraireferme);
		
		mv.setViewName("espaceTransporteur");
		
		return mv;
	}
	
	@GetMapping(value = {"deleteRelaisById/{relais.id}"})
	public ModelAndView deleteRelaisById(ModelAndView mv, @PathVariable(value = "relais.id") String id) {
		
		gestionRelais.suppRelais(id);
		
		listeRelais = gestionRelais.listRelais();
		
		mv.addObject("listeRelais", listeRelais);
		mv.setViewName("listeRelais");
		return mv;
	}
	
	@GetMapping(value = { "/updateRelaisById/{relais.id}" })
	public ModelAndView updateRelaisById(ModelAndView mv, @PathVariable(value = "relais.id") String id) {
		
		mv.addObject("",id);
		mv.setViewName("updateRelais");
		return mv;
	}
	
	@PostMapping(value = { "/updateRelais" })
	public ModelAndView updateRelais(ModelAndView mv,
			 @RequestParam(value = "relais_id") String id,
			@RequestParam(value = "adresse") String adresse,
			@RequestParam(value = "ville") String ville,
			@RequestParam(value = "telephone") String telephone,
			@RequestParam(value = "code") String code,
			@RequestParam(value = "pays") String pays,
			@RequestParam(value = "horraireouvert") String openhours,
			@RequestParam(value = "horraireferme") String closedhours) {
		
		LocalTime horraireouvert =  LocalTime.parse(openhours);
		LocalTime horraireferme =  LocalTime.parse(closedhours);
		
		
		gestionRelais.modifRelais(id, adresse, code, ville, pays, telephone, horraireouvert, horraireferme);
		
		
		listeRelais = gestionRelais.listRelais();
		
		mv.addObject("listeRelais", listeRelais);
		mv.setViewName("listeRelais");
		//mv.setViewName("espaceTransporteur");
		
		return mv;
	}
	
	@GetMapping(value = {"/addRelaisToLivraisonById/{relais.id}"})
	public ModelAndView addRelaisLivraison(ModelAndView mv, @PathVariable(value = "relais.id") String id) {
		listeLivraison = (ArrayList<Livraison>) gestionTransporteurController.listLivraison();
		
		mv.addObject("listeLivraison", listeLivraison);
		mv.setViewName("LivraisonToRelais");
		return mv;
	}
	
	
}
