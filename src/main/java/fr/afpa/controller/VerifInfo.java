package fr.afpa.controller;

public class VerifInfo {
	
	
	
	
	public VerifInfo() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @author Gregory 
	 * Valide l'adresse mail saisie.
	 */
	
	public boolean validationEmail( String email ) throws Exception {
		boolean mailvalide = true;
	    if ( email != null && email.trim().length() != 0 ) {
	        if ( !email.matches( "([^.@]+)(\\.[^.@]+)*@([^.@]+\\.)+([^.@]+)" ) ) {
	        	mailvalide = false;
	            throw new Exception( "Merci de saisir une adresse mail valide." );
	            
	        }
	    } else {
	    	mailvalide = false;
	        throw new Exception( "Merci de saisir une adresse mail." );
	    }
	    
	    return mailvalide;
	}

	/**
	 * @author Gregory 
	 * Valide les mots de passe saisis.
	 */
	
	public boolean validationMotsDePasse( String motDePasse, String confirmation ) throws Exception{
		boolean valide = true;
	    if (motDePasse != null && motDePasse.trim().length() != 0 && confirmation != null && confirmation.trim().length() != 0) {
	        if (!motDePasse.equals(confirmation)) {
	        	valide = false;
	            throw new Exception("Les mots de passe entrés sont différents, merci de les saisir à nouveau.");
	        } else if (motDePasse.trim().length() < 3) {
	        	valide = false;
	            throw new Exception("Les mots de passe doivent contenir au moins 3 caractères.");
	        }
	    } else {
	        throw new Exception("Merci de saisir et confirmer votre mot de passe.");
	    }
	    return valide;
	}

	/**
	 * @author Gregory 
	 * Valide le nom d'utilisateur saisi.
	 */
	
	public boolean validationNom( String nom ) throws Exception {
		boolean nomValide = true;
	    if ( nom != null && nom.trim().length() < 3 ) {
	    	nomValide = false;
	        throw new Exception( "Le nom d'utilisateur doit contenir au moins 3 caractères." );
	    }
	    return nomValide;
	}
	
	
	public boolean validationPhone( String phone ) throws Exception {
		boolean phonevalide = true;
	    if ( phone != null && phone.trim().length() != 0 ) {
	        if ( !phone.matches( "^(?:(?:\\+|00)33|0)\\s*[1-9](?:[\\s.-]*\\d{2}){4}$" ) ) {
	        	phonevalide = false;
	            throw new Exception( "Merci de saisir un numéro de tel valide." );
	            
	        }
	    } else {
	    	phonevalide = false;
	        throw new Exception( "Merci de saisir un numéro de tel." );
	    }
	    
	    return phonevalide;
	}
	

}
