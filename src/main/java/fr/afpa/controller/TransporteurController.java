package fr.afpa.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import fr.afpa.beans.Compte;
import fr.afpa.beans.Transporteur;
import fr.afpa.constants.Constants;
import fr.afpa.model.GestionClient;
import fr.afpa.model.GestionTransporteur;
import fr.afpa.repository.dao.CompteRepository;
import fr.afpa.repository.dao.TransporteurRepository;

@Controller
@Transactional
public class TransporteurController implements Constants {
	
	@Autowired
	private GestionTransporteur gestionTransporteur;
	private Compte compte;
	private Transporteur transporteur;
	
	
	@GetMapping(value = {"/"})
	public ModelAndView redirectionAccueil(ModelAndView mv)  {
		mv.setViewName(accueil);
		return mv;
	}
	
	@GetMapping(value = {"/creationCompte"})
	public ModelAndView redirectionCreationCompte(ModelAndView mv)  {
		mv.setViewName("creationCompte");
		return mv;
	}
	
	@PostMapping(value = {"/CreationTransporteur"})
	public ModelAndView addPersonne(@RequestParam(value = "nom") String nom,
			@RequestParam(value = "prenom") String prenom,
			@RequestParam(value = "mail") String mail,
			@RequestParam(value = "login") String login,
			@RequestParam(value = "password") String password,
			@RequestParam(value = "confirmpassword") String confirmpassword,
			@RequestParam(value = "noPhone") String telephone,
			ModelAndView mv) throws Exception {
		VerifInfo verif = new VerifInfo();
		String resultat = new String();
		boolean mailvalide = false;
		boolean passvalide = false;
		boolean loginvalide = false;
		boolean phonevalide = false;
		Map<String, String> erreurs = new HashMap<String, String>();
				
		/*  On vérifie si le mail est valide*/
		try {
			mailvalide = verif.validationEmail(mail);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			erreurs.put( "mail", e.getMessage() );
		}
		
		 /* Validation des champs mot de passe et confirmation. */
        try {
            passvalide = verif.validationMotsDePasse( password, confirmpassword );
        } catch ( Exception e ) {
            erreurs.put( "password", e.getMessage() );
        }
        
        /* Validation du champ login. */
        try {
        	loginvalide =  verif.validationNom( login );
        } catch ( Exception e ) {
            erreurs.put( "log", e.getMessage() );
        }
        
        /* Validation du champ  phone. */
        try {
        	phonevalide =  verif.validationPhone( telephone );
        } catch ( Exception e ) {
            erreurs.put( "noPhone", e.getMessage() );
        }
				
		 /* Initialisation du résultat global de la validation. */
        if ( erreurs.isEmpty() ) {
        	gestionTransporteur.addTransporteur(prenom, prenom, mail, telephone, login, password);
            resultat = "Succès de l'inscription.";
        } else {
            resultat = "Échec de l'inscription.";
        }
                
      
		mv.addObject("erreurs",erreurs);
		mv.addObject("resultat",resultat);
		mv.setViewName("creationCompte");
		return mv;
	}
	
	
	
	
	@GetMapping(value = {"Connexion"})
	public ModelAndView connexion(ModelAndView mv)  {
		mv.setViewName("Connexion");
		return mv;
	}
	
	@GetMapping(value = {"Deconnexion"})
	public ModelAndView deconnexion(ModelAndView mv,HttpSession session)  {
		
		session.invalidate();
		mv.setViewName("accueil");
		return mv;
	}
	
	
	@PostMapping(value = {"/ConnexionTransporteur"})
	public ModelAndView connexionTranspo(ModelAndView mv,@RequestParam(value = "login") String login,
			@RequestParam(value = "password") String password,HttpSession session) throws Exception {
		
		String resultat = new String();
		String auth = new String();
		transporteur = new Transporteur();
		auth = gestionTransporteur.authTransporteur(login, password);
		
		if(auth !="auth reussi") {
			  resultat = "Login ou mot de passe incorrect";
			  mv.addObject("resultat",resultat);
			  mv.setViewName("Connexion");
		}
		
		if(auth =="auth reussi") {
			session.setAttribute("transporteur", gestionTransporteur.getTransporteurAuth());
			mv.addObject(session);
			mv.setViewName("espaceTransporteur");
			}
		
		return mv;
	}
	
	
	@GetMapping(value = {"redirectionPageGestionUser"})
	public ModelAndView listclient(ModelAndView mv) {
		
		mv.setViewName("listeClient");
		
		return mv;
	}
	

	@GetMapping(value = {"espaceTranspo"})
	public ModelAndView espaceMembre(ModelAndView mv) {
		
		mv.setViewName("espaceTransporteur");
		
		return mv;
	}
	
}
