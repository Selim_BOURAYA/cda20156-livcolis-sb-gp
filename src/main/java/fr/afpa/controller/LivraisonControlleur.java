package fr.afpa.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import fr.afpa.beans.Colis;
import fr.afpa.beans.HeureLivraison;
import fr.afpa.beans.Livraison;
import fr.afpa.beans.Relais;
import fr.afpa.constants.Constants;
import fr.afpa.model.GestionColis;
import fr.afpa.model.GestionRelais;
import fr.afpa.model.GestionTransporteur;
import fr.afpa.repository.dao.HeureLivRepository;

@Controller
@Transactional
public class LivraisonControlleur implements Constants {
	
	@Autowired
	private GestionColis gestionColisController;
	
	@Autowired
	private GestionRelais gestionRelais;
	@Autowired
	private GestionTransporteur gestionTransporteurController;
	private List <Livraison> listeLivraison;
	private Livraison livraison;
	private String view;
	private Colis colis;
	private Set<Relais> listeRelais;
	private Set<HeureLivraison> listeStatut;
	private List <HeureLivraison> listeStatutSorted;
	private List <Relais> listeRelaisSorted;
	private HeureLivraison heureLivraison;
	private ArrayList<Relais> listeRelais1;
	private Relais relaisLiv;
	private Relais relaisLivinter;
	private Relais relaisLivdfin;

	
	@GetMapping(value ={"/addcolis", "/deleteColisbyId/addcolis"})
	public ModelAndView formulaireAddcolis(ModelAndView mv){
		listeRelais1 = gestionRelais.listRelais();
		
		mv.addObject("listeRelais", listeRelais1);
		mv.setViewName("AddColis");
		return mv;
	}
		
	/*
	 * 
	 * @Greg
	 * */
	@PostMapping(value = {"/addcolis"})
	public ModelAndView addcolis(@RequestParam(value = "poids") String poids, 
									@RequestParam(value = "idEmetteur") String idEmetteur,
									@RequestParam(value = "idDestinataire") String idDestinataire,
									@RequestParam(value = "choixRelais") String relaisInter,
									@RequestParam(value = "choixRelaisfirst") String relais1,
									@RequestParam(value = "choixRelaisfin") String relais2,
									ModelAndView mv) {
		
		//on ajoute le colis dans la BDD (une id est une sequenceColis sont générées à l'enregistrement
		gestionColisController.addColisController(idEmetteur, idDestinataire, poids);
		
		//on instancie la liste de relais nécessaire à la construction de la livraison
		listeRelais = new HashSet <Relais>();
		// on récupérere les information des relais depuis la BDD afin d'instancier des objets relais
		relaisLiv = gestionRelais.getRelais(relais1);
		relaisLivinter = gestionRelais.getRelais(relaisInter);
		relaisLivdfin = gestionRelais.getRelais(relais2);
		
		// on ajoute les relais à la liste de relais
		listeRelais.add(relaisLiv);
		listeRelais.add(relaisLivinter);
		listeRelais.add(relaisLivdfin);
		
		// on récupérere le colis (avec l'id et le codecolis du colis nouvellement généré) précedemment enregistré dans la BDD grâce à l'id emetteur
		colis = gestionColisController.getColisEmetteur(idEmetteur);
		
		// on appelle la méthode de construction de la livraison
		gestionTransporteurController.buildLivraison(""+colis.getId(), listeRelais);
		
		// on récupére la nouvelle liste de livrasion du transporteur
		listeLivraison = gestionTransporteurController.listLivraison();
				
		livraison = gestionTransporteurController.getLivraison();
		
		gestionTransporteurController.envoiMail(livraison, relaisLiv,colis.getEmetteur().getMail(),colis.getDestinataire().getMail());
				
		mv.addObject("listeLivraison", listeLivraison);
		mv.setViewName("listeColis");
		return mv;
	}
	
	
	@GetMapping(value = { "/listecolis"})
	public ModelAndView viewListColis(ModelAndView mv) {
		
		listeLivraison = gestionTransporteurController.listLivraison();
		
		mv.addObject("listeLivraison", listeLivraison);
		mv.setViewName("listeColis");
		return mv;
	}
	
	/**
	 * @author Selim
	 * méthode d'affichage de la liste de suivi d'une livraison, à partir de son id. Les HashSet <HeureLivraison> et <Relais> sont 
	 * transformé et trié par id via un stream
	 */
	@GetMapping(value ="/parcourscolis/{id}")
	public ModelAndView parcoursColis(@PathVariable String id, ModelAndView mv) {
		
		livraison = gestionTransporteurController.getLivraison(id);
		
		// on trie les listes grâce à un stream trié à partir de l'id de l'entité
		if(livraison != null) {
						
		listeRelais = livraison.getListeRelais();
		listeRelaisSorted = listeRelais.stream().sorted(Comparator.comparing(Relais::getId)).collect(Collectors.toList());
		
		listeStatut = livraison.getListeHeureReception();
		listeStatutSorted = listeStatut.stream().sorted(Comparator.comparing(HeureLivraison::getId)).collect(Collectors.toList());
		}
		
		view = parcoursColis;
				
		mv.addObject("listeStatut",listeStatutSorted);
		mv.addObject("livraison",livraison);
		mv.addObject("listeRelais",listeRelaisSorted);
				
		mv.setViewName(view);
		return mv;
	}
	
		
	@GetMapping(value = { "/reception/{id}" })
	public ModelAndView modifStatutLivraison(@PathVariable String id, ModelAndView mv) {
		
		livraison = gestionTransporteurController.getLivraison(id);
		gestionTransporteurController.modifStatut(livraison);
		
		return parcoursColis(""+livraison.getId(), mv);
	}
	
	@GetMapping(value = {"redirectionPageEspaceTransporteur"})
	public ModelAndView espaceTransporteur(ModelAndView mv) {
		
		mv.setViewName("espaceTransporteur");
		
		return mv;
	}
	
	
	@GetMapping(value = {"deleteColisbyId/{livraison.colis.id}"})
	public ModelAndView deleteColisById1(ModelAndView mv, @PathVariable(value = "livraison.colis.id") String id){
		
		gestionTransporteurController.suppLivraison(id);
//		listeLivraison = gestionTransporteurController.listLivraison();
//		
//		mv.addObject("listeLivraison", listeLivraison);
//		mv.setViewName("listeColis");
		
		return viewListColis(mv);
	}
	
	@GetMapping(value = { "/AddRelaisLivraisonById/{livraison.id}" })
	public ModelAndView addReaistoLiv(
			@PathVariable(value = "livraison.id") String id1,ModelAndView mv) {
	
	/*Relais relais = gestionRelais.getRelais(id1);
	
	gestionTransporteurController.addRelaisToLivraison(relais, colis);*/
	
	mv.setViewName("espaceTransporteur");
		
		return mv;
	}
	
	@GetMapping(value = {"/deleteColisById/${livraison.id}"})
	public ModelAndView deleteColisById(ModelAndView mv, @PathVariable(value = "livraison.colis.id") String id) {
		
		gestionTransporteurController.suppLivraison(id);
		
//		listeLivraison = gestionTransporteurController.listLivraison();
//		
//		mv.addObject("listeLivraison", listeLivraison);
//		mv.setViewName("listeColis");
		return viewListColis(mv);
		}
	
	
}
