package fr.afpa.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import fr.afpa.beans.Client;
import fr.afpa.beans.Colis;
import fr.afpa.model.GestionColis;
import fr.afpa.repository.dao.ClientRepository;
import fr.afpa.repository.dao.ColisRepository;

@Controller
public class ColisController {
	
	@Autowired
	private GestionColis gestionColis;
	
	@Autowired
	private ClientRepository clientRepo;
	
	@GetMapping(value = {"redirectionPageGestionColis"})
	public ModelAndView redirectionGestionColis(ModelAndView mv) {
		mv.setViewName("gestionColis");
	return mv;	
	}
	
	@GetMapping(value = {"redirectionPageAddColis"})
	public ModelAndView redirectAddColis(ModelAndView mv) {
		mv.setViewName("AddColis");
		return mv;
	}
	
	
	
}
