package fr.afpa.controller;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import fr.afpa.beans.Client;
import fr.afpa.beans.HeureLivraison;
import fr.afpa.beans.Livraison;
import fr.afpa.beans.Relais;
import fr.afpa.constants.Constants;
import fr.afpa.model.GestionClient;

@Controller
@Transactional
public class ClientController implements Constants {

	@Autowired
	private GestionClient gestionClientController;
	private Client client;
	private Livraison livraison;
	private ArrayList<Client> listeClient;
	private Set<Relais> listeRelais;
	private Set<HeureLivraison> listeStatut;
	private List<HeureLivraison> listeStatutSorted;
	private List<Relais> listeRelaisSorted;
	private String view;
	private static final Logger logger = LoggerFactory.getLogger(ClientController.class);

	/**
	 * @author Selim appel de la page "addUser" lorsque l'url est renseigné avec
	 *         "/addclient" ou "/supp/addclient"
	 *
	 */
	@GetMapping(value = { "/addclient", "/supp/addclient" })
	public String addclient() {
		return "AddUser";
	}

	/**
	 * @author Selim récupère les informations renseignées dans le formulaire de la
	 *         page "addUser" afin de les traiter via la méthode addClient
	 *
	 */
	@PostMapping(value = { "/addclient" })
	public ModelAndView addclient(@RequestParam(value = "prenom") String prenom,
			@RequestParam(value = "prenom") String nom, @RequestParam(value = "mail") String mail,
			@RequestParam(value = "telephone") String telephone, ModelAndView mv) {

		gestionClientController.addClient(prenom, nom, mail, telephone);

		return viewListClient(mv);
	}

	/**
	 * author Selim récupère la liste des clients et renvoi l'affichage de cette
	 * liste
	 *
	 */
	@GetMapping(value = { "/listeclient" })
	public ModelAndView viewListClient(ModelAndView mv) {

		listeClient = gestionClientController.listClient();

		mv.addObject("listeClient", listeClient);
		mv.setViewName("listeClient");
		return mv;

	}

	@GetMapping(value = { "/mod/{id}" })
	public ModelAndView modifierDepuisListe(ModelAndView mv, @PathVariable String id) {

		client = gestionClientController.getClient(id);
		mv.addObject("client", client);
		mv.setViewName("updateUser");

		return mv;
	}

	@PostMapping(value = "/updateClient")
	public ModelAndView formModifierClient(@RequestParam(value = "id") String id,
			@RequestParam(value = "prenom") String prenom, @RequestParam(value = "prenom") String nom,
			@RequestParam(value = "mail") String mail, @RequestParam(value = "telephone") String telephone,
			ModelAndView mv) {

		view = gestionClientController.modifClient(id, prenom, nom, mail, telephone);

		listeClient = gestionClientController.listClient();

		mv.addObject("listeClient", listeClient);
		mv.setViewName("listeClient");

		return mv;
	}

	@GetMapping(value = { "/supp/{id}" })
	public ModelAndView supprimerClient(ModelAndView mv, @PathVariable String id) {

		view = gestionClientController.suppClient(id);

		listeClient = gestionClientController.listClient();

		mv.addObject("listeClient", listeClient);
		mv.setViewName("listeClient");
		return mv;
	}

	/**
	 * @author Selim méthode renvoyant et affichant les informations liées à la
	 *         livraison depuis la page d'accueil et renvoi vers la page
	 *         "parcoursColisClient"
	 */
	@PostMapping(value = { "recherche" })
	public ModelAndView findColis(ModelAndView mv, @RequestParam(value = "codecolis") String codeClient) {

		livraison = gestionClientController.getLivraisonClient(codeClient);

		// on trie les listes grâce à un stream trié à partir de l'id de l'entité
		if (livraison != null) {

			listeRelais = livraison.getListeRelais();
			listeRelaisSorted = listeRelais.stream().sorted(Comparator.comparing(Relais::getId))
					.collect(Collectors.toList());

			listeStatut = livraison.getListeHeureReception();
			listeStatutSorted = listeStatut.stream().sorted(Comparator.comparing(HeureLivraison::getId))
					.collect(Collectors.toList());
		}

		view = parcoursColisClient;

		mv.addObject("listeStatut", listeStatutSorted);
		mv.addObject("livraison", livraison);
		mv.addObject("listeRelais", listeRelaisSorted);

		mv.setViewName(view);
		return mv;
	}

}
